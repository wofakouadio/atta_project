<?php
session_start();
include '../includes/session.php';

?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Fast Food | Admin Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="msapplication-tap-highlight" content="no">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="./main.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/autofill/2.3.7/css/autoFill.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css" />
    <link rel="shortcut icon" href="../img/logo.jpg" type="image/x-icon">

    <style>
        .app-footer__inner .nav .nav-item .nav-link {
            color: white;
        }
    </style>

</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <div class="app-header header-shadow">
            <div class="app-header__logo">
                <div class="logo-src"></div>
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>
                    <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6"></i>
                        </span>
                    </button>
                </span>
            </div>
            <div class="app-header__content">

                <div class="app-header-right">
                    <div class="header-btn-lg pr-0">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="btn-group">
                                        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                            <img width="42" class="rounded-circle" src="assets/images/avatars/1.jpg" alt="">
                                            <i class="fa fa-angle-down ml-2 opacity-12 text-light"></i>
                                        </a>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                            <a tabindex="0" class="dropdown-item"><?php echo $_SESSION["role"]; ?> <i class="pe-7s-user"></i></a>
                                            <div tabindex="-1" class="dropdown-divider"></div>
                                            <a tabindex="0" class="dropdown-item" href="../logout?user=Administrator">Logout <i class="pe-7s-door-lock"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="app-main">
            <div class="app-sidebar sidebar-shadow">
                <div class="app-header__logo">
                    <div class="logo-src">Fast Food</div>
                    <div class="header__pane ml-auto">
                        <div>
                            <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="app-header__mobile-menu">
                    <div>
                        <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
                <div class="app-header__menu">
                    <span>
                        <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                            <span class="btn-icon-wrapper">
                                <i class="fa fa-ellipsis-v fa-w-6"></i>
                            </span>
                        </button>
                    </span>
                </div>
                <div class="scrollbar-sidebar">
                    <div class="app-sidebar__inner">
                        <ul class="vertical-nav-menu">

                            <li>
                                <a href="index">
                                    <i class="metismenu-icon pe-7s-rocket"></i>
                                    Dashboard
                                </a>
                            </li>

                            <li>
                                <a href="">
                                    <i class="metismenu-icon pe-7s-diamond"></i>
                                    Categories
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <li>
                                        <a href="create_category">
                                            <i class="metismenu-icon"></i>
                                            Create
                                        </a>
                                    </li>
                                    <li>
                                        <a href="view_categories">
                                            <i class="metismenu-icon"></i>
                                            View
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="">
                                    <i class="metismenu-icon pe-7s-folder"></i>
                                    Food Menu
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <li>
                                        <a href="create_food_menu">
                                            <i class="metismenu-icon"></i>
                                            Create
                                        </a>
                                    </li>
                                    <li>
                                        <a href="view_food_menu">
                                            <i class="metismenu-icon"></i>
                                            View
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="">
                                    <i class="metismenu-icon pe-7s-car"></i>
                                    Orders
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <!-- <li>
                                        <a href="create_order">
                                            <i class="metismenu-icon"></i>
                                            Create
                                        </a>
                                    </li> -->
                                    <li>
                                        <a href="view_orders" class="mm-active">
                                            <i class="metismenu-icon"></i>
                                            View
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="">
                                    <i class="metismenu-icon pe-7s-bookmarks"></i>
                                    Receipts
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <!-- <li>
                                        <a href="create_receipt">
                                            <i class="metismenu-icon"></i>
                                            Create
                                        </a>
                                    </li> -->
                                    <li>
                                        <a href="view_receipts">
                                            <i class="metismenu-icon"></i>
                                            View
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="">
                                    <i class="metismenu-icon pe-7s-graph3"></i>
                                    Reports
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <!-- <li>
                                        <a href="create_report">
                                            <i class="metismenu-icon"></i>
                                            Create
                                        </a>
                                    </li> -->
                                    <li>
                                        <a href="view_reports">
                                            <i class="metismenu-icon"></i>
                                            View
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>

            <!-- Main Content Page -->
            <div class="app-main__outer">

                <!-- Content Page -->
                <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-rocket icon-gradient bg-mean-fruit">
                                    </i>
                                </div>
                                <div>Orders List</div>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col">
                            <div class="main-card mb-3 card">
                                <div class="card-body">
                                    <h5 class="card-title">Table View</h5>
                                    <div id="msg"></div>
                                    <div class="table-responsive">
                                        <table class="mb-0 table" id="OrdersTable_View">
                                            <thead>
                                                <tr>
                                                    <th>Order ID</th>
                                                    <th>Name</th>
                                                    <th>Contact</th>
                                                    <th>Shipping Address</th>
                                                    <th>Payment Mode</th>
                                                    <th>Items</th>
                                                    <th>Amount</th>
                                                    <th>Ordered Date</th>
                                                    <th>Status</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Order ID</th>
                                                    <th>Name</th>
                                                    <th>Contact</th>
                                                    <th>Shipping Address</th>
                                                    <th>Payment Mode</th>
                                                    <th>Items</th>
                                                    <th>Amount</th>
                                                    <th>Ordered Date</th>
                                                    <th>Status</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- Content Page End -->


                <!-- footer start-->
                <div class="app-wrapper-footer">
                    <div class="app-footer">
                        <div class="app-footer__inner">
                            <div class="app-footer-left">
                                <ul class="nav">
                                    <li class="nav-item">
                                        <a href="javascript:void(0);" class="nav-link">
                                            &copy All rights reserved 2021
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="javascript:void(0);" class="nav-link">
                                            Campus Fast Food Joint
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="app-footer-right">
                                <ul class="nav">
                                    <li class="nav-item">
                                        <a href="javascript:void(0);" class="nav-link">
                                            Pentecost University
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer end-->

            </div>

            <!-- Main Content Page -->

            <!-- <script src="http://maps.google.com/maps/api/js?sensor=true"></script> -->
        </div>
    </div>

    <!-- Modals -->

    <!-- Edit Food Menu Status Modal -->
    <div class="modal fade" id="EditOrderStatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h3 class="modal-title text-light" id="exampleModalLabel">Order Status</h3>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" class="form-group">
                    <div class="modal-body">

                        <div class="form-group text-center">
                            <h6 id="S_order" class="display-6"></h6>
                            <input type="hidden" name="s_order_id" value="" id="S_order_id">
                            <div id="S_order_status"></div><br />
                            <input type="radio" name="C_order_status" value="1"><span class="badge bg-info text-light text-uppercase">ready</span>
                            <input type="radio" name="C_order_status" value="2"><span class="badge bg-warning text-light text-uppercase">in delivery</span>
                            <input type="radio" name="C_order_status" value="3"><span class="badge bg-success text-light text-uppercase">received</span>
                            <input type="radio" name="C_order_status" value="4"><span class="badge bg-success text-light text-uppercase">picked up</span>
                        </div>

                        <div class="form-group">
                            <input type="text" name="s_order_comments" value="" class="form-control" placeholder="Kindly enter any comments">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary text-light" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary text-light" id="btn-C-OS">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Food Menu Detail Modal -->
    <div class="modal fade" id="EditOrder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-warning">
                    <h3 class="modal-title text-light" id="header">Order Payment Mode</h3>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" class="form-group">
                    <div class="modal-body">

                        <div class="form-group">
                            <input type="hidden" name="p_order_id" value="" id="P_order_id">
                            <input type="text" name="p_client_name" id="P_client_name" class="form-control" value="" readonly>
                        </div>
                        <div class="form-group">
                            <input type="text" name="p_client_contact" value="" id="P_client_contact" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <input type="text" name="p_client_del_add" value="" id="P_client_del_add" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" style="height:100px;" id="P_items" readonly></textarea>
                        </div>
                        <div class="form-group">
                            <input type="text" name="p_total_price" value="" id="P_total_price" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <input type="text" name="p_ordered_date" value="" id="P_ordered_date" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <p id="P_payment_mode"></p>
                            <select class="form-control" id="n_payment_mode">
                                <option value="0">Choose</option>
                                <option value="1">MoMo</option>
                                <option value="2">Credit Card</option>
                                <option value="3">Cash on delivery</option>
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary text-light" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-warning text-light">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Delete Food Menu Detail Modal -->
    <div class="modal fade" id="DeleteOrder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h3 class="modal-title text-light" id="exampleModalLabel">Order Cancellation</h3>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" class="form-group">
                    <div class="modal-body">

                        <div class="form-group">
                            <h6 id="delete_content" class="display-6"></h6>
                            <input type="hidden" name="d_order_id" value="" id="D_order_id">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary text-light" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-danger text-light" id="btn-D-O">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modals -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/autofill/2.3.7/js/dataTables.autoFill.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables-buttons/1.7.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="./assets/scripts/main.js"></script>

    <!-- custom javascript -->
    <script>
        $(document).ready(function() {

            $(".alert").fadeTo(2000, 500).slideUp(2000, function() {
                $(".alert").slideUp(500);
            });

            // get orders records from database in html table and arrange and formatting using DATATABLES scripting to have a nice output of the raw data
            $("#OrdersTable_View").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                processing: true,
                serverSide: true,
                ajax: 'js/orders_view',
                order: [
                    [0, 'DESC']
                ],
                autowidth: false
            });

            // change order status using bootstrap modal and ajaxRequest
            $("#EditOrderStatus").on("shown.bs.modal", function(event) {
                // getting data from modal show event when modal displays
                // because when the button linked to this modal is clicked, the variables below are tied to it allowing JS to get the data and manipulate as wish
                var str = $(event.relatedTarget);

                var order_id = str.data("order_id");
                var client_name = str.data("client_name");
                var client_contact = str.data("client_contact");
                var client_delivery_address = str.data("client_delivery_address");
                var payment_mode = str.data("payment_mode");
                var items = str.data("items");
                var total_price = str.data("total_price");
                var ordered_date = str.data("ordered_date");
                var order_status = str.data("order_status");

                var modal = $(this);

                // console.log(order_status)

                if (order_status == 0) {
                    order_status = "<span class='badge bg-danger text-uppercase text-light'>placed</span>";
                } else if (order_status == 1) {
                    order_status = "<span class='badge bg-info text-uppercase text-light'>ready</span>";
                } else if (order_status == 2) {
                    order_status = "<span class='badge bg-warning text-uppercase text-light'>in delivery</span>";
                } else if (order_status == 3) {
                    order_status = "<span class='badge bg-success text-uppercase text-light'>received</span>";
                } else {
                    order_status = "<span class='badge bg-success text-uppercase text-light'>picked up</span>";
                }

                modal.find("#S_order").html("<b>" + client_name + "</b> order");
                modal.find("#S_order_id").val(order_id);
                modal.find("#S_order_status").html("Status : " + order_status);

            });

            // change order payment mode using bootstrap modal and ajax
            $("#EditOrder").on("shown.bs.modal", function(event) {

                var str = $(event.relatedTarget);

                var order_id = str.data("order_id");
                var client_name = str.data("client_name");
                var client_contact = str.data("client_contact");
                var client_delivery_address = str.data("client_delivery_address");
                var payment_mode = str.data("payment_mode");
                var items = str.data("items");
                var total_price = str.data("total_price");
                var ordered_date = str.data("ordered_date");
                var order_status = str.data("order_status");

                var modal = $(this);

                modal.find("#header").html("<b>" + client_name + "</b> order");
                modal.find("#p_order_id").val(order_id);
                modal.find("#P_client_name").val(client_name);
                modal.find("#P_client_contact").val(client_contact);
                modal.find("#P_client_del_add").val(client_delivery_address);
                modal.find("#P_items").val(items);
                modal.find("#P_total_price").val(total_price);
                modal.find("#P_ordered_date").val(ordered_date);
                modal.find("#P_payment_mode").html('Payment mode chosen : ' + payment_mode);

            });

            // delete order using bootstrap modal and ajax
            $("#DeleteOrder").on("shown.bs.modal", function(event) {

                var str = $(event.relatedTarget);

                var order_id = str.data("order_id");
                var client_name = str.data("client_name");
                var client_contact = str.data("client_contact");
                var client_delivery_address = str.data("client_delivery_address");
                var payment_mode = str.data("payment_mode");
                var items = str.data("items");
                var total_price = str.data("total_price");
                var ordered_date = str.data("ordered_date");
                var order_status = str.data("order_status");

                var modal = $(this);

                modal.find("#delete_content").html("Are you sure of deleting <b>" + client_name + "</b> order?");
                modal.find("#D_order_id").val(order_id);

            });

            // change order status in bootstrap modal using ajax request
            $("#btn-C-OS").click(function() {

                var order_status = $("input[name='C_order_status']:checked").val();
                var order_id = $("input[name='s_order_id']").val();
                var order_comments = $("input[name='s_order_comments']").val();

                $.ajax({

                    url: 'js/ajaxRequest',
                    method: 'POST',
                    cache: false,
                    data: {
                        change_order_status: 'change_order_status',
                        order_status: order_status,
                        order_id: order_id,
                        order_comments: order_comments
                    },
                    success: function(ChangeOrderStatus_Response) {

                        $("#msg").html(ChangeOrderStatus_Response);
                        $("#EditOrderStatus").modal('hide');
                        $("#OrdersTable_View").DataTable().ajax.reload();

                    }

                });

            });

            // delete order in bootstrap modal using ajax request
            $("#btn-D-O").click(function() {

                var order_id = $("input[name='d_order_id']").val();
                // console.log(order_id);

                $.ajax({

                    url: 'js/ajaxRequest',
                    method: 'POST',
                    cache: false,
                    data: {
                        delete_order: 'delete_order',
                        order_id: order_id
                    },
                    success: function(DeleteOrderData_Response) {

                        $("#msg").html(DeleteOrderData_Response);
                        $("#DeleteOrder").modal('hide');
                        $("#OrdersTable_View").DataTable().ajax.reload();
                    }

                });

            });


        });
    </script>
</body>

</html>