<?php
session_start();
include '../includes/session.php';

?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Fast Food | Admin Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="main.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/autofill/2.3.7/css/autoFill.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css" />
    <link rel="shortcut icon" href="../img/logo.jpg" type="image/x-icon">

    <style>
        .btn {
            color: black;
            /* background-color: #f6f6f6; */
            border-color: #683a16;
        }

        .btn :hover {
            background-color: #683a16;
            color: white;
        }
    </style>

</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <div class="app-header header-shadow">
            <div class="app-header__logo">
                <div class="logo-src"></div>
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>
                    <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6"></i>
                        </span>
                    </button>
                </span>
            </div>
            <div class="app-header__content">

                <div class="app-header-right">
                    <div class="header-btn-lg pr-0">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="btn-group">
                                        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                            <img width="42" class="rounded-circle" src="assets/images/avatars/1.jpg" alt="">
                                            <i class="fa fa-angle-down ml-2 opacity-12 text-light"></i>
                                        </a>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                            <a tabindex="0" class="dropdown-item"><?php echo $_SESSION["role"]; ?> <i class="pe-7s-user"></i></a>
                                            <div tabindex="-1" class="dropdown-divider"></div>
                                            <a tabindex="0" class="dropdown-item" href="../logout?user=Administrator">Logout <i class="pe-7s-door-lock"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="app-main">
            <div class="app-sidebar sidebar-shadow">
                <div class="app-header__logo">
                    <div class="logo-src">Fast Food</div>
                    <div class="header__pane ml-auto">
                        <div>
                            <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="app-header__mobile-menu">
                    <div>
                        <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
                <div class="app-header__menu">
                    <span>
                        <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                            <span class="btn-icon-wrapper">
                                <i class="fa fa-ellipsis-v fa-w-6"></i>
                            </span>
                        </button>
                    </span>
                </div>
                <div class="scrollbar-sidebar">
                    <div class="app-sidebar__inner">
                        <ul class="vertical-nav-menu">

                            <li>
                                <a href="index">
                                    <i class="metismenu-icon pe-7s-rocket"></i>
                                    Dashboard
                                </a>
                            </li>

                            <li>
                                <a href="">
                                    <i class="metismenu-icon pe-7s-diamond"></i>
                                    Categories
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <li>
                                        <a href="create_category">
                                            <i class="metismenu-icon"></i>
                                            Create
                                        </a>
                                    </li>
                                    <li>
                                        <a href="view_categories">
                                            <i class="metismenu-icon"></i>
                                            View
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="">
                                    <i class="metismenu-icon pe-7s-folder"></i>
                                    Food Menu
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <li>
                                        <a href="create_food_menu">
                                            <i class="metismenu-icon"></i>
                                            Create
                                        </a>
                                    </li>
                                    <li>
                                        <a href="view_food_menu" class="mm-active">
                                            <i class="metismenu-icon"></i>
                                            View
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="">
                                    <i class="metismenu-icon pe-7s-car"></i>
                                    Orders
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <!-- <li>
                                        <a href="create_order">
                                            <i class="metismenu-icon"></i>
                                            Create
                                        </a>
                                    </li> -->
                                    <li>
                                        <a href="view_orders">
                                            <i class="metismenu-icon"></i>
                                            View
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="">
                                    <i class="metismenu-icon pe-7s-bookmarks"></i>
                                    Receipts
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <!-- <li>
                                        <a href="create_receipt">
                                            <i class="metismenu-icon"></i>
                                            Create
                                        </a>
                                    </li> -->
                                    <li>
                                        <a href="view_receipts">
                                            <i class="metismenu-icon"></i>
                                            View
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="">
                                    <i class="metismenu-icon pe-7s-graph3"></i>
                                    Reports
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <!-- <li>
                                        <a href="create_report">
                                            <i class="metismenu-icon"></i>
                                            Create
                                        </a>
                                    </li> -->
                                    <li>
                                        <a href="view_reports">
                                            <i class="metismenu-icon"></i>
                                            View
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>

            <!-- Main Content Page -->
            <div class="app-main__outer">

                <!-- Content Page -->
                <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-folder icon-gradient bg-mean-fruit">
                                    </i>
                                </div>
                                <div>Food Menu</div>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col">
                            <div class="main-card mb-3 card">
                                <div class="card-body">
                                    <h5 class="card-title">Table View</h5>
                                    <div class="table-responsive">
                                        <table class="mb-0 table" id="FMTable_View">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Description</th>
                                                    <th>Profile</th>
                                                    <th>Price</th>
                                                    <th>Category</th>
                                                    <th>Status</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Description</th>
                                                    <th>Profile</th>
                                                    <th>Price</th>
                                                    <th>Category</th>
                                                    <th>Status</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- Content Page End -->


                <!-- footer start-->
                <div class="app-wrapper-footer">
                    <div class="app-footer">
                        <div class="app-footer__inner">
                            <div class="app-footer-left">
                                <ul class="nav">
                                    <li class="nav-item">
                                        <a href="javascript:void(0);" class="nav-link">
                                            &copy All rights reserved 2021
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="javascript:void(0);" class="nav-link">
                                            Campus Fast Food Joint
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="app-footer-right">
                                <ul class="nav">
                                    <li class="nav-item">
                                        <a href="javascript:void(0);" class="nav-link">
                                            Pentecost University
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer end-->

            </div>

            <!-- Main Content Page -->

            <!-- <script src="http://maps.google.com/maps/api/js?sensor=true"></script> -->
        </div>
    </div>


    <!-- Edit Food Menu Status Modal -->
    <div class="modal fade" id="EditFoodMenuStatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h3 class="modal-title" id="exampleModalLabel">Food Menu Status</h3>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" class="form-group">
                    <div class="modal-body">

                        <div class="form-group text-center">
                            <h6 id="S_fm_name" class="display-6"></h6>
                            <input type="hidden" name="s_fm_id" value="" id="S_fm_id">
                            <div id="S_fm_status"></div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary text-light" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary text-light">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Food Menu Detail Modal -->
    <div class="modal fade" id="EditFoodMenu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-warning">
                    <h3 class="modal-title text-light" id="exampleModalLabel">Food Menu Information Update</h3>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" class="form-group">
                    <div class="modal-body">

                        <div class="form-group">
                            <input type="hidden" name="s_fm_id" value="" id="S_fm_id">
                            <input type="text" name="s_fm_name" id="S_fm_name" class="form-control" value="">
                        </div>
                        <div class="form-group">
                            <input type="text" name="s_fm_price" value="" id="S_fm_price" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="text" name="s_fm_description" value="" id="S_fm_description" class="form-control">
                        </div>
                        <div class="form-group" id="S_fm_image"></div>
                        <div class="form-group">
                            <input type="file" name="s_fm_image" value="" id="N_fm_image" class="form-control">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary text-light" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-warning text-light">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Delete Food Menu Detail Modal -->
    <div class="modal fade" id="DeleteFoodMenu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h3 class="modal-title text-light" id="exampleModalLabel">Food Menu Information Deletion</h3>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" class="form-group">
                    <div class="modal-body">

                        <div class="form-group">
                            <h6 id="S_fm_name" class="display-6"></h6>
                            <input type="hidden" name="s_fm_id" value="" id="S_fm_id">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary text-light" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-danger text-light">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/autofill/2.3.7/js/dataTables.autoFill.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables-buttons/1.7.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="assets/scripts/main.js"></script>

    <!-- custom javascript -->
    <script>
        $(document).ready(function() {

            $("#FMTable_View").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                processing: true,
                serverSide: true,
                ajax: 'js/fm_view',
            });

            // edit food menu status in bootstrap modal using ajax
            $("#EditFoodMenuStatus").on("shown.bs.modal", function(event) {

                var str = $(event.relatedTarget);

                var fm_name = str.data("fm_name");
                var fm_id = str.data("fm_id");
                var fm_status = str.data("fm_status");

                console.log("Test :" + fm_name + " " + fm_id + " " + fm_status);

                var modal = $(this);

                if (fm_status == 0) {
                    // fm_status = "Status = Available";
                    fm_status = "Status = <span class='badge bg-success text-uppercase'>Available</span> </br> <input type='radio' name='s_fm_name' id='S_fm_name_1' value='1'> Unavailable";
                } else {
                    fm_status = "Unavailable";
                }

                modal.find('#S_fm_name').html("Change <b>" + fm_name + "</b> Status");
                modal.find('#S_fm_id').val(fm_id);
                modal.find('#S_fm_status').html(fm_status);

            });

            //delete food menu in bootstrap modal
            $("#DeleteFoodMenu").on("shown.bs.modal", function(event) {

                var str = $(event.relatedTarget);

                var fm_name = str.data("fm_name");
                var fm_id = str.data("fm_id");

                var modal = $(this);

                modal.find('#S_fm_name').html("Are you sure of deleting <b>" + fm_name + "</b> from the menu?");
                modal.find('#S_fm_id').val(fm_id);

            });


            $('#EditFoodMenu').on('shown.bs.modal', function(event) {

                var str = $(event.relatedTarget);

                var fm_name = str.data("fm_name");
                var fm_id = str.data("fm_id");
                var fm_description = str.data("fm_description");
                var fm_image = str.data("fm_image");
                var fm_price = str.data("fm_price");
                var category_id = str.data("category_id");
                var category_name = str.data("category_name");

                var modal = $(this);

                modal.find("#S_fm_id").val(fm_id);
                modal.find("#S_fm_name").val(fm_name);
                modal.find("#S_fm_price").val(fm_price);
                modal.find("#S_fm_description").val(fm_description);
                modal.find("#S_fm_image").html("<img src='../img/" + fm_image + "' class='img' width='100px'>")

            });

        });
    </script>
</body>

</html>