<?php
session_start();
include '../includes/session.php';
include '../database/config.php';
include '../admin/js/dashboard_api.php';
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Fast Food | Admin Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="msapplication-tap-highlight" content="no">
    <link href="./main.css" rel="stylesheet">
    <link rel="shortcut icon" href="../img/logo.jpg" type="image/x-icon">

    <!-- custom stylesheet -->
    <style>
        .btn {
            color: black;
            /* background-color: #f6f6f6; */
            border-color: #683a16;
        }

        .btn:hover {
            background-color: #683a16;
        }
    </style>

</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <div class="app-header header-shadow">
            <div class="app-header__logo">
                <div class="logo-src"></div>
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>
                    <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6"></i>
                        </span>
                    </button>
                </span>
            </div>
            <div class="app-header__content">

                <div class="app-header-right">
                    <div class="header-btn-lg pr-0">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="btn-group">
                                        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                            <img width="42" class="rounded-circle" src="assets/images/avatars/1.jpg" alt="">
                                            <i class="fa fa-angle-down ml-2 opacity-12 text-light"></i>
                                        </a>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                            <a tabindex="0" class="dropdown-item"><?php echo $_SESSION["role"]; ?> <i class="pe-7s-user"></i></a>
                                            <div tabindex="-1" class="dropdown-divider"></div>
                                            <a tabindex="0" class="dropdown-item" href="../logout?user=Administrator">Logout <i class="pe-7s-door-lock"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="app-main">
            <div class="app-sidebar sidebar-shadow">
                <div class="app-header__logo">
                    <div class="logo-src">Fast Food</div>
                    <div class="header__pane ml-auto">
                        <div>
                            <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="app-header__mobile-menu">
                    <div>
                        <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
                <div class="app-header__menu">
                    <span>
                        <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                            <span class="btn-icon-wrapper">
                                <i class="fa fa-ellipsis-v fa-w-6"></i>
                            </span>
                        </button>
                    </span>
                </div>
                <div class="scrollbar-sidebar">
                    <div class="app-sidebar__inner">
                        <ul class="vertical-nav-menu">

                            <li>
                                <a href="index" class="mm-active">
                                    <i class="metismenu-icon pe-7s-rocket"></i>
                                    Dashboard
                                </a>
                            </li>

                            <li>
                                <a href="">
                                    <i class="metismenu-icon pe-7s-diamond"></i>
                                    Categories
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <li>
                                        <a href="create_category">
                                            <i class="metismenu-icon"></i>
                                            Create
                                        </a>
                                    </li>
                                    <li>
                                        <a href="view_categories">
                                            <i class="metismenu-icon"></i>
                                            View
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="">
                                    <i class="metismenu-icon pe-7s-folder"></i>
                                    Food Menu
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <li>
                                        <a href="create_food_menu">
                                            <i class="metismenu-icon"></i>
                                            Create
                                        </a>
                                    </li>
                                    <li>
                                        <a href="view_food_menu">
                                            <i class="metismenu-icon"></i>
                                            View
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="">
                                    <i class="metismenu-icon pe-7s-car"></i>
                                    Orders
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <!-- <li>
                                        <a href="create_order">
                                            <i class="metismenu-icon"></i>
                                            Create
                                        </a>
                                    </li> -->
                                    <li>
                                        <a href="view_orders">
                                            <i class="metismenu-icon"></i>
                                            View
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="">
                                    <i class="metismenu-icon pe-7s-bookmarks"></i>
                                    Receipts
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <!-- <li>
                                        <a href="create_receipt">
                                            <i class="metismenu-icon"></i>
                                            Create
                                        </a>
                                    </li> -->
                                    <li>
                                        <a href="view_receipts">
                                            <i class="metismenu-icon"></i>
                                            View
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="">
                                    <i class="metismenu-icon pe-7s-graph3"></i>
                                    Reports
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <!-- <li>
                                        <a href="create_report">
                                            <i class="metismenu-icon"></i>
                                            Create
                                        </a>
                                    </li> -->
                                    <li>
                                        <a href="view_reports">
                                            <i class="metismenu-icon"></i>
                                            View
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>

            <!-- Main Content Page -->
            <div class="app-main__outer">

                <!-- Content Page -->
                <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-rocket icon-gradient bg-mean-fruit">
                                    </i>
                                </div>
                                <div>Dashboard</div>
                            </div>
                        </div>
                    </div>

                    <div class="main-card mb-3 card">
                        <div class="no-gutters row">
                            <div class="col-md-4">
                                <div class="pt-0 pb-0 card-body">
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item">
                                            <div class="widget-content p-0">
                                                <div class="widget-content-outer">
                                                    <div class="widget-content-wrapper">
                                                        <div class="widget-content-left">
                                                            <div class="widget-heading">Categories</div>
                                                            <!-- <div class="widget-subheading">Last year expenses</div> -->
                                                        </div>
                                                        <div class="widget-content-right">
                                                            <div class="widget-numbers text-success"><?php echo $get_cats; ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="widget-content p-0">
                                                <div class="widget-content-outer">
                                                    <div class="widget-content-wrapper">
                                                        <div class="widget-content-left">
                                                            <div class="widget-heading">Menu Items</div>
                                                            <!-- <div class="widget-subheading">Total Clients Profit</div> -->
                                                        </div>
                                                        <div class="widget-content-right">
                                                            <div class="widget-numbers text-primary"><?php echo $get_menu_items; ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="pt-0 pb-0 card-body">
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item">
                                            <div class="widget-content p-0">
                                                <div class="widget-content-outer">
                                                    <div class="widget-content-wrapper">
                                                        <div class="widget-content-left">
                                                            <div class="widget-heading">Orders</div>
                                                            <div class="widget-subheading">Daily</div>
                                                        </div>
                                                        <div class="widget-content-right">
                                                            <div class="widget-numbers text-danger"><?php echo $get_daily_orders; ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="widget-content p-0">
                                                <div class="widget-content-outer">
                                                    <div class="widget-content-wrapper">
                                                        <div class="widget-content-left">
                                                            <div class="widget-heading">Food Sold</div>
                                                            <div class="widget-subheading">Total revenue streams</div>
                                                        </div>
                                                        <div class="widget-content-right">
                                                            <div class="widget-numbers text-warning">GHc <?php echo $get_total_revenue; ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="pt-0 pb-0 card-body">
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item">
                                            <div class="widget-content p-0">
                                                <div class="widget-content-outer">
                                                    <div class="widget-content-wrapper">
                                                        <div class="widget-content-left">
                                                            <div class="widget-heading">Total Orders</div>
                                                            <!-- <div class="widget-subheading">Last year expenses</div> -->
                                                        </div>
                                                        <div class="widget-content-right">
                                                            <div class="widget-numbers text-success"><?php echo $get_total_orders; ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="widget-content p-0">
                                                <div class="widget-content-outer">
                                                    <div class="widget-content-wrapper">
                                                        <div class="widget-content-left">
                                                            <div class="widget-heading">Administrator</div>
                                                            <!-- <div class="widget-subheading">Total Clients Profit</div> -->
                                                        </div>
                                                        <div class="widget-content-right">
                                                            <div class="widget-numbers text-primary">01</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Content Page End -->


                <!-- footer start-->
                <div class="app-wrapper-footer">
                    <div class="app-footer">
                        <div class="app-footer__inner">
                            <div class="app-footer-left">
                                <ul class="nav">
                                    <li class="nav-item">
                                        <a href="javascript:void(0);" class="nav-link">
                                            &copy All rights reserved 2021
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="javascript:void(0);" class="nav-link">
                                            Campus Fast Food Joint
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="app-footer-right">
                                <ul class="nav">
                                    <li class="nav-item">
                                        <a href="javascript:void(0);" class="nav-link">
                                            Pentecost University
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer end-->

            </div>

            <!-- Main Content Page -->

            <!-- <script src="http://maps.google.com/maps/api/js?sensor=true"></script> -->
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script type="text/javascript" src="./assets/scripts/main.js"></script>
</body>

</html>