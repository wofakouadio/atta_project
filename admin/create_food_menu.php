<?php
session_start();
include '../includes/session.php';
include '../database/config.php';

//function to implement security features by trim   ming any input 
function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

$msg = "";


//adding food menu in food menus tble
if (isset($_POST["createFM_btn"])) {

    // function to  create food menu id
    function FMID($db_link)
    {

        $fetch_fms_sql = "SELECT * FROM food_menus";

        $fetch_fms_exe = mysqli_query($db_link, $fetch_fms_sql);

        if ($fetch_fms_exe) {

            if (mysqli_num_rows($fetch_fms_exe) == 0) {

                $count_from = 1;
                $fm_ID = "FM" . sprintf("%05d", $count_from);
            }

            if (mysqli_num_rows($fetch_fms_exe) > 0) {

                $fetch_lastFM_sql = "SELECT fm_id FROM food_menus ORDER BY fm_id DESC LIMIT 1";
                $fetch_lastFM_exe = mysqli_query($db_link, $fetch_lastFM_sql);
                $fetch_lastFM_res = mysqli_fetch_array($fetch_lastFM_exe);
                $fetch_lastFM = $fetch_lastFM_res["fm_id"];
                $explode = substr($fetch_lastFM, -5);
                $count_from = "$explode" + 1;
                $fm_ID = "FM" . sprintf("%05d", $count_from);
            }

            return $fm_ID;
        }
    }

    //assigning values to variables
    $category_id = test_input($_POST["category_id"]);
    $food_name = test_input(ucwords($_POST["food_name"]));
    $food_price = test_input($_POST["food_price"]);
    $food_description = test_input($_POST["food_description"]);

    $fm_id = FMID($db_link);

    // create compress image function based on image format type
    function compressImage($source_food_image, $compress_image)
    {

        $image_info = getimagesize($source_food_image);

        if ($image_info["mime"] == "image/jpeg") {

            $source_food_image = imagecreatefromjpeg($source_food_image);
            imagejpeg($source_food_image, $compress_image, 35);
        } elseif ($image_info["mime"] == "image/gif") {

            $source_food_image = imagecreatefromgif($source_food_image);
            imagegif($source_food_image, $compress_image, 35);
        } elseif ($image_info["mime"] == "image/png") {

            $source_food_image = imagecreatefrompng($source_food_image);
            imagepng($source_food_image, $compress_image, 6);
        }

        return $compress_image;
    }

    $food_image_Name = $_FILES["food_image"]["name"];
    $food_image_TmpName = $_FILES["food_image"]["tmp_name"];
    $directory_name = "../img/";

    $ext = explode(".", $food_image_Name);

    $valid_ext = strtolower(end($ext));

    $food_image_NewName = $fm_id . "." . $valid_ext;

    $file_name = $directory_name . $food_image_Name;

    move_uploaded_file($food_image_TmpName, $file_name);

    $compress_file = $food_image_NewName;

    $compressed_img = $directory_name . $compress_file;

    $compress_image = compressImage($file_name, $compressed_img);

    unlink($file_name);


    // insert food menu into food menu table in database
    $ins_food_menu = "INSERT INTO food_menus (fm_id, fm_name, fm_description, fm_image, fm_price, cat_id, fm_status) VALUES ('$fm_id', '$food_name', '$food_description', '$food_image_NewName',  '$food_price', '$category_id', 0)";

    $exe_food_menu = mysqli_query($db_link, $ins_food_menu);

    if ($exe_food_menu) {

        $msg = "<div class='alert alert-success'>" . $food_name . " menu created successful</div>";
    } else {

        $msg = "<div class='alert alert-success'>" . $food_name . " menu failed registration</div>";
    }
}


?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Fast Food | Admin Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="msapplication-tap-highlight" content="no">
    <link href="./main.css" rel="stylesheet">
    <link rel="shortcut icon" href="../img/logo.jpg" type="image/x-icon">
    <!-- custom stylesheet -->
    <style>
        .btn {
            color: black;
            /* background-color: #f6f6f6; */
            border-color: #683a16;
        }

        .btn:hover {
            background-color: #683a16;
            color: white;
        }
    </style>
</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <div class="app-header header-shadow">
            <div class="app-header__logo">
                <div class="logo-src"></div>
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>
                    <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6"></i>
                        </span>
                    </button>
                </span>
            </div>
            <div class="app-header__content">

                <div class="app-header-right">
                    <div class="header-btn-lg pr-0">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="btn-group">
                                        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                            <img width="42" class="rounded-circle" src="assets/images/avatars/1.jpg" alt="">
                                            <i class="fa fa-angle-down ml-2 opacity-12 text-light"></i>
                                        </a>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                            <a tabindex="0" class="dropdown-item"><?php echo $_SESSION["role"]; ?> <i class="pe-7s-user"></i></a>
                                            <div tabindex="-1" class="dropdown-divider"></div>
                                            <a tabindex="0" class="dropdown-item" href="../logout?user=Administrator">Logout <i class="pe-7s-door-lock"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="app-main">
            <div class="app-sidebar sidebar-shadow">
                <div class="app-header__logo">
                    <div class="logo-src">Fast Food</div>
                    <div class="header__pane ml-auto">
                        <div>
                            <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="app-header__mobile-menu">
                    <div>
                        <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
                <div class="app-header__menu">
                    <span>
                        <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                            <span class="btn-icon-wrapper">
                                <i class="fa fa-ellipsis-v fa-w-6"></i>
                            </span>
                        </button>
                    </span>
                </div>
                <div class="scrollbar-sidebar">
                    <div class="app-sidebar__inner">
                        <ul class="vertical-nav-menu">

                            <li>
                                <a href="index">
                                    <i class="metismenu-icon pe-7s-rocket"></i>
                                    Dashboard
                                </a>
                            </li>

                            <li>
                                <a href="">
                                    <i class="metismenu-icon pe-7s-diamond"></i>
                                    Categories
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <li>
                                        <a href="create_category">
                                            <i class="metismenu-icon"></i>
                                            Create
                                        </a>
                                    </li>
                                    <li>
                                        <a href="view_categories">
                                            <i class="metismenu-icon"></i>
                                            View
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="">
                                    <i class="metismenu-icon pe-7s-folder"></i>
                                    Food Menu
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <li>
                                        <a href="create_food_menu" class="mm-active">
                                            <i class="metismenu-icon"></i>
                                            Create
                                        </a>
                                    </li>
                                    <li>
                                        <a href="view_food_menu">
                                            <i class="metismenu-icon"></i>
                                            View
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="">
                                    <i class="metismenu-icon pe-7s-car"></i>
                                    Orders
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <!-- <li>
                                        <a href="create_order">
                                            <i class="metismenu-icon"></i>
                                            Create
                                        </a>
                                    </li> -->
                                    <li>
                                        <a href="view_orders">
                                            <i class="metismenu-icon"></i>
                                            View
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="">
                                    <i class="metismenu-icon pe-7s-bookmarks"></i>
                                    Receipts
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <!-- <li>
                                        <a href="create_receipt">
                                            <i class="metismenu-icon"></i>
                                            Create
                                        </a>
                                    </li> -->
                                    <li>
                                        <a href="view_receipts">
                                            <i class="metismenu-icon"></i>
                                            View
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="">
                                    <i class="metismenu-icon pe-7s-graph3"></i>
                                    Reports
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <!-- <li>
                                        <a href="create_report">
                                            <i class="metismenu-icon"></i>
                                            Create
                                        </a>
                                    </li> -->
                                    <li>
                                        <a href="view_reports">
                                            <i class="metismenu-icon"></i>
                                            View
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>

            <!-- Main Content Page -->
            <div class="app-main__outer">

                <!-- Content Page -->
                <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-folder icon-gradient bg-mean-fruit">
                                    </i>
                                </div>
                                <div>Food Menu</div>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col main-card mb-3 card">
                            <div class="card-body">
                                <h5 class="card-title">Create Food Menu</h5>
                                <div id="msg"> <?php echo $msg; ?></div>
                                <form class="form" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" enctype="multipart/form-data">

                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <div class="position-relative form-group">
                                                <label for="categoryNAME" class="">Category</label>
                                                <select class="form-control" name="category_id" id="categoryID">
                                                    <option value="0">Select Food Category</option>
                                                    <?php
                                                    $fetch_Cats_sql = "SELECT * FROM categories ORDER BY category_name ASC";
                                                    $fetch_Cats_exe = mysqli_query($db_link, $fetch_Cats_sql);
                                                    while ($fetch_Cats_res = mysqli_fetch_array($fetch_Cats_exe)) {

                                                        echo "<option value='" . $fetch_Cats_res['category_id'] . "'>" . $fetch_Cats_res['category_name'] . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="position-relative form-group">
                                                <label for="foodname" class="">Food Name</label>
                                                <input name="food_name" id="foodName" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <div class="position-relative form-group">
                                                <label for="foodimage" class="">Image</label>
                                                <input name="food_image" id="foodImage" type="file" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="position-relative form-group">
                                                <label for="foodprice" class="">Price</label>
                                                <input name="food_price" id="foodPrice" type="number" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="position-relative form-group">
                                            <label for="fooddescription" class="">Description</label>
                                            <textarea name="food_description" id="foodDescription" cols="30" rows="10" class="form-control-file"></textarea>
                                        </div>
                                    </div>

                                    <button class="mt-2 btn" id="btn_create_food" type="submit" name="createFM_btn">Create <i class="pe-7s-pen"></i></button>

                                </form>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- Content Page End -->


                <!-- footer start-->
                <div class="app-wrapper-footer">
                    <div class="app-footer">
                        <div class="app-footer__inner">
                            <div class="app-footer-left">
                                <ul class="nav">
                                    <li class="nav-item">
                                        <a href="javascript:void(0);" class="nav-link">
                                            &copy All rights reserved 2021
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="javascript:void(0);" class="nav-link">
                                            Campus Fast Food Joint
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="app-footer-right">
                                <ul class="nav">
                                    <li class="nav-item">
                                        <a href="javascript:void(0);" class="nav-link">
                                            Pentecost University
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer end-->

            </div>

            <!-- Main Content Page -->

            <!-- <script src="http://maps.google.com/maps/api/js?sensor=true"></script> -->
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script type="text/javascript" src="./assets/scripts/main.js"></script>

    <!-- custom javascript -->


    <!--End of Tawk.to Script-->
    <script>
        $(document).ready(function() {

            // <!--Start of Tawk.to Script-->
            // var Tawk_API = Tawk_API || {},
            //     Tawk_LoadStart = new Date();
            // (function() {
            //     var s1 = document.createElement("script"),
            //         s0 = document.getElementsByTagName("script")[0];
            //     s1.async = true;
            //     s1.src = 'https://embed.tawk.to/60eb6728649e0a0a5ccbacff/1fabolufq';
            //     s1.charset = 'UTF-8';
            //     s1.setAttribute('crossorigin', '*');
            //     s0.parentNode.insertBefore(s1, s0);
            // })();

            // $("#btn_create_food").click(function() {

            // var category_id = $("#categoryID").val();
            // var food_name = $("#foodName").val();
            // var food_image = $("#foodImage").val();
            // var food_price = $("#foodPrice").val();
            // var food_description = $("#foodDescription").val();

            // // console.warn(category_id);

            // if (category_id == "Select Food Category") {

            //     document.getElementById("categoryID").style.border = "2px solid red";
            //     $("#categoryID").focus();
            //     return false;
            // } else {
            //     document.getElementById("categoryID").style.border = "2px solid green";
            // }

            // if (food_name == "") {

            //     document.getElementById("foodName").style.border = "2px solid red";
            //     $("#foodName").focus();
            //     return false;
            // } else {
            //     document.getElementById("foodName").style.border = "2px solid green";
            // }

            // if (food_price == "") {
            //     document.getElementById("foodPrice").style.border = "2px solid red";
            //     $("#foodPrice").focus();
            //     return false;
            // } else {

            //     food_image = $("#foodPrice").val();
            //     document.getElementById("foodPrice").style.border = "2px solid green";
            // }

            //ajax script to handle food menu insertion in db via php script
            // $.ajax({

            //     url: 'js/ajaxRequest',
            //     method: 'POST',
            //     cache: false,
            //     contentType: false,
            //     processData: false,
            //     data: {
            //         CreateFM: 'CreateFM',
            //         category_id: category_id,
            //         food_name: food_name,
            //         food_image: food_image,
            //         food_price: food_price,
            //         food_description: food_description
            //     },  
            //     success: function(FMResult) {
            //         $("#msg").html(FMResult);
            //         $("#MenFood_Form")[0].reset();
            //     }

            // });

            // });

        });
    </script>
</body>

</html>