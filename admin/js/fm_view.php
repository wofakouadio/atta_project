<?php

//Server side processing millions of records from db in micro second
include "../../database/config.php";

$sql_details = array(
    'host' => HOSTNAME,
    'user' => USERNAME,
    'pass' => PASSWORD,
    'db' => DATABASE
);


$table = "food_menus";

$primary_key = "fm_id";

$columns = array(

    array('db' => '`fm`.`fm_name`', 'dt' => 0, 'field' => 'fm_name'),
    array('db' => '`fm`.`fm_description`', 'dt' => 1, 'field' => 'fm_description'),
    array('db' => '`fm`.`fm_image`', 'dt' => 2, 'formatter' => function ($d, $row) {
        $image = $row['fm_image'];
        $fm_image = '<img src="../../img/' . $image . '" alt="' . $image . '" width=50px height=50px class="img">';
        return $fm_image;
    }, 'field' => 'fm_image'),
    array('db' => '`fm`.`fm_price`', 'dt' => 3, 'field' => 'fm_price'),
    array('db' => '`cat`.`category_name`', 'dt' => 4, 'field' => 'category_name'),
    array('db' => '`fm`.`fm_status`', 'dt' => 5, 'formatter' => function ($d, $row) {

        if ($row["fm_status"] == 0) {

            $status = "<span class='badge bg-success text-uppercase text-light'>active</span>";
        } else {

            $status = "<span class='badge bg-danger text-uppercase text-light'>unavailable</span>";
        }
        return $status;
    }, 'field' => 'fm_status'),
    array('db' => '`fm`.`fm_id`, `fm`.`fm_name`, `fm`.`fm_description`, `fm`.`fm_image`, `fm`.`fm_price`, `cat`.`category_id`, `cat`.`category_name`', 'dt' => 6, 'formatter' => function ($d, $row) {
        return '
                    <a class="btn btn-info text-light" data-bs-toggle="modal" data-bs-target="#EditFoodMenuStatus" data-fm_id="' . $row["fm_id"] . '" data-fm_name="' . $row["fm_name"] . '" data-fm_status="' . $row["fm_status"] . '"><i class="fa fa-check-double"></i></a>
                    
                    <a class="btn btn-warning text-light" data-bs-toggle="modal" data-bs-target="#EditFoodMenu" data-fm_id="' . $row["fm_id"] . '" data-fm_name="' . $row["fm_name"] . '" data-fm_description="' . $row["fm_description"] . '" data-fm_image="' . $row["fm_image"] . '" data-fm_price="' . $row["fm_price"] . '" data-cat_id= "' . $row["category_id"] . '" data-cat_name="' . $row["category_name"] . '"><i class="fa fa-pen"></i></a>
                    
                    <a class="btn btn-danger text-light" data-bs-toggle="modal" data-bs-target="#DeleteFoodMenu" data-fm_id="' . $row["fm_id"] . '" data-fm_name="' . $row["fm_name"] . '"><i class="fa fa-trash"></i></a>
                ';
    }, 'field' => 'fm_id')

);

require('ssp.class.php');
$joinQuery = "FROM `food_menus` `fm` JOIN `categories` `cat` ON `fm`.`cat_id` = `cat`.`category_id`";
echo json_encode(
    SSP::simple($_GET, $sql_details, $table, $primary_key, $columns, $joinQuery)
);
