<?php

//Server side processing millions of records from db in micro second
include "../../database/config.php";

$sql_details = array(
    'host' => HOSTNAME,
    'user' => USERNAME,
    'pass' => PASSWORD,
    'db' => DATABASE
);


$table = "orders";

$primary_key = "order_id";

$columns = array(

    array('db' => '`order_id`', 'dt' => 0, 'field' => 'order_id'),
    array('db' => '`ordered_date`', 'dt' => 1, 'field' => 'ordered_date'),
    array('db' => '`client_name`', 'dt' => 2, 'field' => 'client_name'),
    array('db' => '`client_contact`', 'dt' => 3, 'field' => 'client_contact'),
    array('db' => '`total_price`', 'dt' => 4, 'field' => 'total_price'),
    array('db' => '`order_id`', 'dt' => 5, 'formatter' => function ($d, $row) {
        $order_id = $row['order_id'];
        // $client_name = $row['client_name'];
        // $client_contact = $row['client_contact'];
        // $total_price = $row['total_price'];
        // $ordered_date = $row['ordered_date'];
        return '
                    <a class="btn btn-sm btn-success text-light" href="../../receipts/billing?utm=' . $order_id . '" target="_blank"><i class="pe-7s-download"></i></a>
                    
                ';
    }, 'field' => 'order_id')

);

require('ssp.class.php');
$joinQuery = "FROM `orders`";
echo json_encode(
    SSP::simple($_GET, $sql_details, $table, $primary_key, $columns, $joinQuery)
);
