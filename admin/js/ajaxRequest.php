<?php
// start session
session_start();
// inserting session status if session holds session variables
include('../../includes/session.php');
// include database connection string
include('../../database/config.php');

//function to sanitize data by trimming any input 
function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}


//create category script

if (isset($_POST['action'])) {

    if (isset($_POST['action']) == 'Create_Cat') {

        //function to create count based id
        function CatID($db_link)
        {

            $fetch_Cats_sql = "SELECT * FROM categories";

            $fetch_Cats_exe = mysqli_query($db_link, $fetch_Cats_sql);

            if ($fetch_Cats_exe) {

                if (mysqli_num_rows($fetch_Cats_exe) == 0) {
                    $count_from = 1;
                    $Cat_ID = "CAT" . sprintf("%04d", $count_from);
                }

                if (mysqli_num_rows($fetch_Cats_exe) > 0) {
                    $fetch_lastCat_sql = "SELECT category_id FROM categories ORDER BY category_id DESC LIMIT 1";
                    $fetch_lastCat_exe = mysqli_query($db_link, $fetch_lastCat_sql);
                    $fetch_lastCat_res = mysqli_fetch_array($fetch_lastCat_exe);
                    $fetch_lastCat = $fetch_lastCat_res["category_id"];
                    $explode = substr($fetch_lastCat, -4);
                    $count_from = "$explode" + 1;
                    $Cat_ID = "CAT" . sprintf("%04d", $count_from);
                }
            }
            return $Cat_ID;
        }

        $category_name = test_input(ucwords($_POST['category_name']));
        $category_id = test_input(CatID($db_link));

        $ins_Cat = "INSERT INTO categories(category_id, category_name) VALUES('$category_id', '$category_name')";

        $ins_Cat_exe = mysqli_query($db_link, $ins_Cat);

        if ($ins_Cat_exe) {

            $output = "<div class='alert alert-success'>" . $category_name . " Category created successful</div>";
        } else {

            $output = "<div class='alert alert-warning'>" . $category_name . " Category failed registration.</div>";
        }

        echo $output;
    }
}


// change order status script using data submitted by ajax request
if (isset($_POST['change_order_status'])) {

    if (isset($_POST['change_order_status']) == 'change_order_status') {

        $order_id = test_input($_POST["order_id"]);
        $order_status = test_input($_POST["order_status"]);
        $order_comments = test_input($_POST["order_comments"]);

        // updating order status in orders table
        $up_order_status_sql = "UPDATE orders SET order_status = '$order_status' WHERE order_id = '$order_id'";
        $up_order_status_exe = mysqli_query($db_link, $up_order_status_sql);

        // insert updated order status in order tracking table
        $ins_order_tracking_sql = "INSERT INTO order_tracking(order_id, tracking_status, tracking_comments, tracking_date, tracking_time) VALUES('$order_id', '$order_status', '$order_comments', NOW(), NOW())";
        $ins_order_tracking_exe = mysqli_query($db_link, $ins_order_tracking_sql);

        // check if all queries have been executed
        if ($up_order_status_exe && $ins_order_tracking_exe) {
            $output = "<div class='alert alert-success'> Order <b>#" . $order_id . "</b> status updated successful</div>";
        } else {
            $output = "<div class='alert alert-danger'> Order <b>#" . $order_id . "</b> status update failed" . mysqli_error($db_link) . "</div>";
        }

        echo $output;
    }
}

// delete order data from bootstrap modal using ajax request sent to this form
if (isset($_POST['delete_order'])) {

    if (isset($_POST['delete_order']) == 'delete_order') {

        $order_id = test_input($_POST['order_id']);

        // delete from orders table
        $del_order_data_sql = "DELETE FROM orders WHERE order_id = '$order_id'";
        // delete from order tracking table
        $del_order_tracking_data_sql = "DELETE FROM order_tracking WHERE order_id = '$order_id'";

        // execute all queries and check if they are all well executed
        if (mysqli_query($db_link, $del_order_data_sql) && (mysqli_query($db_link, $del_order_tracking_data_sql))) {

            $output = "<div class='alert alert-success'> Order <b>#" . $order_id . "</b> data deleted successful</div>";
        } else {
            $output = "<div class='alert alert-danger'> Order <b>#" . $order_id . "</b> data deletion failed" . mysqli_error($db_link) . "</div>";
        }

        echo $output;
    }
}


// getting reports on items sold with date range
if (isset($_POST["getReports"]) && isset($_POST["getReports"]) == "getReports") {

    $from_date = test_input($_POST["from_date"]);
    $to_date = test_input($_POST["to_date"]);
    $report_on = test_input($_POST["report_on"]);

    $total_revenue = 0;
    $x = 0;

    if ($report_on == "Orders") {

        $get_orders_reports_sql = "SELECT * FROM carts c JOIN orders o ON c.user_session_id = o.user_session_id WHERE DATE(o.ordered_date) BETWEEN '$from_date' AND '$to_date'";

        $get_orders_reports_exe = mysqli_query($db_link, $get_orders_reports_sql);

        if ($get_orders_reports_exe) {

            $get_orders_reports_chk = mysqli_num_rows($get_orders_reports_exe);

            if ($get_orders_reports_chk <= 0) {

                $output = "<div class='alert alert-info'>No records available from $from_date to $to_date</div>";
            } else {

                $output = "<h5 class='display-6 text-center'>Items sold from $from_date to $to_date</h5>";
                $output .= "<table class='table table-striped'>";
                $output .= "<thead>";
                $output .= "<tr>";
                $output .= "<th>Client Name</th>";
                $output .= "<th>Client Contact</th>";
                $output .= "<th>Description</th>";
                $output .= "<th>Qty</th>";
                $output .= "<th>Unit Price</th>";
                $output .= "<th>Total</th>";
                $output .= "</tr>";
                $output .= "</thead>";
                $output .= "<tbody>";
                while ($get_orders_reports_row = mysqli_fetch_array($get_orders_reports_exe)) {

                    $client_name = $get_orders_reports_row["client_name"];
                    $client_contact = $get_orders_reports_row["client_contact"];
                    $fm_name = $get_orders_reports_row["fm_name"];
                    $fm_price = $get_orders_reports_row["fm_price"];
                    $fm_quantity = $get_orders_reports_row["fm_quantity"];
                    $fm_total_price = $get_orders_reports_row["fm_total_price"];
                    $x += $fm_total_price;
                    $total_revenue = sprintf('%.2f', $x);


                    $output .= "<tr>
                                    <td>$client_name</td>
                                    <td>$client_contact</td>
                                    <td>$fm_name</td>
                                    <td>$fm_quantity</td>
                                    <td>$fm_price</td>
                                    <td>$fm_total_price</td>
                                </tr>";
                }

                $output .= "<tr>
                                <td colspan='5' class='text-right'>Total Revenue</td>
                                <td><b>$total_revenue</b></td>
                            </tr>";

                $output .= "</tbody>";
                // $output .= "<tfoot>";
                // $output .= "<tr>";
                // $output .= "<th>Client Name</th>";
                // $output .= "<th>Client Contact</th>";
                // $output .= "<th>Description</th>";
                // $output .= "<th>Qty</th>";
                // $output .= "<th>Unit Price</th>";
                // $output .= "<th>Total</th>";
                // $output .= "</tr>";
                // $output .= "</tfoot>";
                $output .= "</table>";
            }
        }
    }

    if ($report_on == "Order Tracking") {
    }

    echo $output;
}
