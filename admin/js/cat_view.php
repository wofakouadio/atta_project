<?php

//Server side processing millions of records from db in micro second
include "../../database/config.php";

$sql_details = array(
    'host' => HOSTNAME,
    'user' => USERNAME,
    'pass' => PASSWORD,
    'db' => DATABASE
);


$table = "categories";

$primary_key = "id";

$columns = array(

    array('db' => '`category_id`', 'dt' => 0, 'field' => 'category_id'),
    array('db' => '`category_name`', 'dt' => 1, 'field' => 'category_name'),
    array('db' => '`category_id`', 'dt' => 2, 'formatter' => function ($d, $row) {
        return '
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#EditCategory" data-ed_cat_id="' . $row["category_id"] . '" data-ed_cat_name="' . $row["category_name"] . '"><i class="fa fa-pen"></i></button>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#DeleteCategory" data-del_cat_id="' . $row["category_id"] . '" data-del_cat_name="' . $row["category_name"] . '"><i class="fa fa-trash"></i></button>
                ';
    }, 'field' => 'category_id')

);

require('ssp.class.php');
$joinQuery = "FROM `categories`";
echo json_encode(
    SSP::simple($_GET, $sql_details, $table, $primary_key, $columns, $joinQuery)
);
