<?php

$today = date("Y-m-d");

// get count of all category items
$get_cats_sql = "SELECT COUNT(category_id) AS TOTAL_COUNT_CAT FROM categories";
$get_cats_exe = mysqli_query($db_link, $get_cats_sql);
$get_cats_res = mysqli_fetch_array($get_cats_exe);

if ($get_cats_res["TOTAL_COUNT_CAT"] <= 0) {

    $get_cats = "00";
} else {

    $get_cats = $get_cats_res["TOTAL_COUNT_CAT"];
}


// get count of all menu items
$get_menu_items_sql = "SELECT COUNT(fm_id) AS TOTAL_COUNT_MI FROM food_menus";
$get_menu_items_exe = mysqli_query($db_link, $get_menu_items_sql);
$get_menu_items_res = mysqli_fetch_array($get_menu_items_exe);

if ($get_menu_items_res["TOTAL_COUNT_MI"] <= 0) {

    $get_menu_items = "00";
} else {

    $get_menu_items = $get_menu_items_res["TOTAL_COUNT_MI"];
}

// get total count of daily orders 
$get_daily_orders_sql = "SELECT COUNT(order_id) AS TOTAL_DAILY_ORDERS FROM orders WHERE DATE(ordered_date) = '$today'";
$get_daily_orders_exe = mysqli_query($db_link, $get_daily_orders_sql);
$get_daily_orders_res = mysqli_fetch_array($get_daily_orders_exe);

if ($get_daily_orders_res["TOTAL_DAILY_ORDERS"] <= 0) {

    $get_daily_orders = "00";
} else {

    $get_daily_orders = $get_daily_orders_res["TOTAL_DAILY_ORDERS"];
}

// get total count of orders
$get_total_orders_sql = "SELECT COUNT(order_id) AS TOTAL_ORDERS FROM orders";
$get_total_orders_exe = mysqli_query($db_link, $get_total_orders_sql);
$get_total_orders_res = mysqli_fetch_array($get_total_orders_exe);

if ($get_total_orders_res["TOTAL_ORDERS"] <= 0) {

    $get_total_orders = "00";
} else {

    $get_total_orders = $get_total_orders_res["TOTAL_ORDERS"];
}

// get total revenue
$get_total_revenue_sql = "SELECT ROUND(SUM(total_price), 2) AS TOTAL_REVENUE FROM orders";
$get_total_revenue_exe = mysqli_query($db_link, $get_total_revenue_sql);
$get_total_revenue_res = mysqli_fetch_array($get_total_revenue_exe);

if ($get_total_revenue_res["TOTAL_REVENUE"] <= 0) {

    $get_total_revenue = "00";
} else {

    $get_total_revenue = $get_total_revenue_res["TOTAL_REVENUE"];
}
