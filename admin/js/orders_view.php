<?php

//Server side processing millions of records from db in micro second
include "../../database/config.php";

$sql_details = array(
    'host' => HOSTNAME,
    'user' => USERNAME,
    'pass' => PASSWORD,
    'db' => DATABASE
);


$table = "orders";

$primary_key = "order_id";

$columns = array(

    array('db' => '`order_id`', 'dt' => 0, 'field' => 'order_id'),
    array('db' => '`client_name`', 'dt' => 1, 'field' => 'client_name'),
    array('db' => '`client_contact`', 'dt' => 2, 'field' => 'client_contact'),
    array('db' => '`client_delivery_address`', 'dt' => 3, 'field' => 'client_delivery_address'),
    array('db' => '`payment_mode`', 'dt' => 4, 'field' => 'payment_mode'),
    array('db' => '`items`', 'dt' => 5, 'field' => 'items'),
    array('db' => '`total_price`', 'dt' => 6, 'field' => 'total_price'),
    array('db' => '`ordered_date`', 'dt' => 7, 'field' => 'ordered_date'),
    array('db' => '`order_status`', 'dt' => 8, 'formatter' => function ($d, $row) {

        if ($row["order_status"] == 0) {

            $status = "<span class='badge bg-danger text-uppercase text-light'>Placed</span>";
        } elseif ($row["order_status"] == 1) {

            $status = "<span class='badge bg-warning text-uppercase text-light'>ready</span>";
        } elseif ($row["order_status"] == 2) {

            $status = "<span class='badge bg-info text-uppercase text-light'>shipped</span>";
        } else {

            $status = "<span class='badge bg-success text-uppercase text-light'>delivered</span>";
        }
        return $status;
    }, 'field' => 'order_status'),
    array('db' => '`order_id`', 'dt' => 9, 'formatter' => function ($d, $row) {
        $order_id = $row['order_id'];
        $client_name = $row['client_name'];
        $client_contact = $row['client_contact'];
        $client_delivery_address = $row['client_delivery_address'];
        $payment_mode = $row['payment_mode'];
        $items = $row['items'];
        $total_price = $row['total_price'];
        $ordered_date = $row['ordered_date'];
        $order_status = $row['order_status'];
        return '
                    <a class="btn btn-sm btn-info text-light" data-bs-toggle="modal" data-bs-target="#EditOrderStatus" data-order_id="' . $order_id . '" data-client_name="' . $client_name . '" data-client_contact="' . $client_contact . '" data-client_delivery_address="' . $client_delivery_address . '" data-payment_mode="' . $payment_mode . '" data-items="' . $items . '" data-total_price="' . $total_price . '" data-ordered_date="' . $ordered_date . '" data-order_status="' . $order_status . '"><i class="fa fa-check-double"></i></a>
                    
                    <a class="btn btn-sm btn-warning text-light" data-bs-toggle="modal" data-bs-target="#EditOrder" data-order_id="' . $order_id . '" data-client_name="' . $client_name . '" data-client_contact="' . $client_contact . '" data-client_delivery_address="' . $client_delivery_address . '" data-payment_mode="' . $payment_mode . '" data-items="' . $items . '" data-total_price="' . $total_price . '" data-ordered_date="' . $ordered_date . '" data-order_status="' . $order_status . '"><i class="fa fa-pen"></i></a>
                    
                    <a class="btn btn-sm btn-danger text-light" data-bs-toggle="modal" data-bs-target="#DeleteOrder" data-order_id="' . $order_id . '" data-client_name="' . $client_name . '" data-client_contact="' . $client_contact . '" data-client_delivery_address="' . $client_delivery_address . '" data-payment_mode="' . $payment_mode . '" data-items="' . $items . '" data-total_price="' . $total_price . '" data-ordered_date="' . $ordered_date . '" data-order_status="' . $order_status . '"><i class="fa fa-trash"></i></a>
                ';
    }, 'field' => 'order_id', 'client_name', 'client_contact', 'client_delivery_address', 'payment_mode', 'items', 'total_price', 'ordered_date', 'order_status')

);

require('ssp.class.php');
$joinQuery = "FROM `orders`";
echo json_encode(
    SSP::simple($_GET, $sql_details, $table, $primary_key, $columns, $joinQuery)
);
