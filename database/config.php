<?php
    //research on define in php
    
    define("HOSTNAME", "localhost");
    define("USERNAME", "root");
    define("PASSWORD", "");
    define("DATABASE", "fast_food_db");
    
    $db_link = mysqli_connect(HOSTNAME, USERNAME, PASSWORD, DATABASE);
    
    if(!$db_link){
        
        echo "<p class='text-center text-danger'><b>System cannot have access to " . DATABASE . ". Please, check your connection script or make sure the server is running or make sure you have internet connectivity. Thanks...</b>". mysqli_error($db_link)."</p>";
        mysqli_close($db_link);
        
    }
