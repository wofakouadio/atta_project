<?php
session_start();
require_once 'database/config.php';

// getting session id
$y = md5(session_id());

$user_session = $_SESSION["user_session "] = $y;

echo "<script>
        console.log('User session : {$_SESSION["user_session "]}');
        </script>";

?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- favicon -->
    <link rel="shortcut icon" href="img/logo.jpg" type="image/x-icon">

    <title>Menu | Fast Food</title>

    <!-- custom stylesheet -->
    <style>
        body {
            margin: 0;
        }

        /* body > #header{position:fixed;} */
        #header {
            width: 100%;
            margin-bottom: 100px;
            position:
                fixed;
            z-index: 9000;
            overflow: auto;
            background: #e6e6e6;
            text-align: center;
            padding: 10px 0;
            transition: all 0.15s linear;
        }

        #header.active {
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.4);
        }

        .header {

            margin-bottom: -35px;
            margin-top: 55.5555px;

        }

        .card {
            /* width: 100%; */
            height: 450px
        }

        .card-img-top {
            height: 220px;
        }

        .card a {
            background-color: #683a16;
            border-color: #683a16;
        }

        .btn-block {
            background-color: #683a16;
            border-color: #683a16;
            color: white;
        }

        .btn-block:hover {
            background-color: #683a16;
            border-color: #683a16;
            color: white;
        }

        .card a:hover {
            background-color: #f6f6f6;
            border-color: #683a16;
            color: #683a16;
        }

        .card-body {
            height: 217px;
            padding-bottom: 0rem;
        }

        .ot {
            text-decoration: none;
        }
    </style>

</head>

<body class="bg-dark">

    <nav class="navbar navbar-expand-lg sticky-top navbar-light bg-light justify-content-between" id="header">
        <a class="navbar-brand mx-3" href=".">
            <img src="img/logo.jpg" alt="" width="30" height="24" class="d-inline-block align-text-top">
            Menu | Fast Food
        </a>
        <!-- <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button> -->
        <a class="btn btn-outline-dark mx-3" href="cart?session=<?php echo $user_session; ?>">
            <input type="hidden" name="user_id" id="user_session_badge" value="<?php echo $user_session; ?>" />
            <i class="fa fa-shopping-cart"></i>
            Cart
            <span id="cart_item_number" class="badge bg-danger">0</span>
        </a>
        <a class="btn btn-outline-dark ot" href="order_tracking">
            Track your order
        </a>
        <!-- <a class="btn btn-outline-dark mx-3" href="login">Login</a> -->
    </nav>

    <div class="container">

        <div class="row m-3">

            <div class="col header">
                <h2 class="text-center text-light">Today's Menu</h2>
                <div id="msg_response" class=""></div>
            </div>

        </div>

        <div class="row align-items-center m-3">

            <?php

            $fetch_menus_sql = "SELECT * FROM food_menus fm JOIN categories c ON fm.cat_id = c.category_id";

            $exe_food_menus = mysqli_query($db_link, $fetch_menus_sql);

            if ($exe_food_menus) {

                if (mysqli_num_rows($exe_food_menus) > 0) {

                    while ($res_food_menus = mysqli_fetch_array($exe_food_menus)) {

                        $food_id = $res_food_menus["fm_id"];
                        $food_name = $res_food_menus["fm_name"];
                        $food_description = $res_food_menus["fm_description"];
                        $food_image = $res_food_menus["fm_image"];
                        // $food_price = sprintf("%.2f", $res_food_menus["fm_price"]);
                        $food_price = number_format($res_food_menus["fm_price"], 2);
                        $category_name = $res_food_menus["category_name"];
                        $category_id = $res_food_menus["category_id"];

            ?>


                        <!-- column one -->
                        <div class="col mt-5 mb-3">

                            <div class="card" style="width: 18rem;">
                                <img src="img/<?php echo $food_image; ?>" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <span class="badge bg-info"><?php echo $category_name; ?></span>
                                    <h5 class="card-title"><?php echo $food_name; ?></h5>
                                    <p class="card-text"><?php echo $food_description; ?></p>
                                    <h6 class="card-title">GHc <?php echo $food_price; ?></h6>

                                    <a href="#" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#ShowFood" data-food_id="<?php echo $food_id; ?>" data-food_name="<?php echo $food_name; ?>" data-food_description="<?php echo $food_description; ?>" data-food_image="<?php echo $food_image; ?>" data-food_price="<?php echo $food_price; ?>" data-category_id="<?php echo $category_id; ?>" data-category_name="<?php echo $category_name; ?>" data-user_session="<?php echo $user_session; ?>"><i class="fa fa-plus-square"></i></a>
                                </div>
                            </div>

                        </div>


            <?php

                    }
                }
            }

            ?>

            <!-- Show food Modal -->
            <div class="modal fade" id="ShowFood" tabindex="-1" aria-labelledby="ShowFoodLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <!-- form to add item to cart -->
                        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" class="form-submit" method="post">
                            <div class="modal-header">
                                <h4 class="modal-title" id="Food_Name_Label">Modal title</h4>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">

                                <div class="form-group">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item active" aria-current="page" id="Category_Name">Home</li>
                                        </ol>
                                    </nav>
                                    <input type="hidden" name="food_id" id="Food_Id">
                                    <input type="hidden" name="category_id" id="Category_Id">
                                    <input type="hidden" name="food_image" id="Food_Image">
                                    <input type="hidden" name="food_name" id="Food_Name">
                                    <input type="hidden" name="user_session" id="User_Session">

                                </div>


                                <div class="form-group">
                                    <label for="">Price</label>
                                    <input type="number" name="food_price" id="Food_Price" class="form-control" readonly="true">
                                </div>

                                <div class="form-group">
                                    <label for="">Quantity</label>
                                    <input type="number" name="food_quantity" id="Food_Quantity" class="form-control" autofocus="true">
                                </div>

                                <div class="form-group">
                                    <label for="">Total Price</label>
                                    <input type="number" name="food_total_price" id="Food_Total_Price" class="form-control" readonly="true">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <!-- <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> -->
                                <input type="button" class="btn btn-block add_food" name="" id="btn_food_id" value="Add to cart">
                            </div>

                        </form>
                    </div>
                </div>
            </div>



        </div>

    </div>


    <!-- Jquery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>

    <!-- custom scripts -->
    <script>
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();
            if (scroll > 0) {
                $("#header").addClass("active");
            } else {
                $("#header").removeClass("active");
            }
        });

        // form reset
        function FormReload() {
            $(".form-submit")[0].reset();
        }

        // when the plus button on each cart is clicked, it triggers this modal to show
        $("#ShowFood").on("show.bs.modal", function(event) {

            FormReload();
            // get the event action and pass it as variable
            var str = $(event.relatedTarget);

            // getting values in variable
            var food_id = str.data("food_id");
            var food_name = str.data("food_name");
            var food_image = str.data("food_image");
            var food_price = str.data("food_price");
            var category_id = str.data("category_id");
            var category_name = str.data("category_name");
            var user_session = str.data("user_session");

            // getting modal form id
            var modal = $(this);

            // putting variables in modal form based on each field id
            modal.find("#Food_Name_Label").html(food_name);
            modal.find("#Category_Name").html(category_name);
            modal.find("#Food_Id").val(food_id);
            modal.find("#Category_Id").val(category_id);
            modal.find("#Food_Image").val(food_image);
            modal.find("#Food_Name").val(food_name);
            parseFloat(modal.find("#Food_Price").val(food_price));
            modal.find("#User_Session").val(user_session);
            // modal.find("#btn_food_id").val(food_id);

            // console.log(food_price);

        });

        // getting total price value in input field

        document.getElementById("Food_Quantity").focus();



        $("#Food_Quantity").keyup(function() {

            var food_quantity = ($("#Food_Quantity").val());
            var food_price = ($("#Food_Price").val());
            // var food_quantity = $("#Food_Quantity").val();
            var food_total_price = ($("#Food_Total_Price").val((food_price * food_quantity).toFixed(2)));
            // console.log(food_total_price);

        })

        // when add cart button is clicked
        $("#btn_food_id").click(function() {

            // e.preventDefault();

            var cart_food_form = $(this).closest(".form-submit");

            var food_id = cart_food_form.find("#Food_Id").val();
            var food_name = cart_food_form.find("#Food_Name").val();
            var food_image = cart_food_form.find("#Food_Image").val();
            var category_id = cart_food_form.find("#Category_Id").val();
            var food_price = cart_food_form.find("#Food_Price").val();
            var food_quantity = cart_food_form.find("#Food_Quantity").val();
            var food_TP = cart_food_form.find("#Food_Total_Price").val();
            var user_session = cart_food_form.find("#User_Session").val();

            $.ajax({

                url: "action.php",
                method: "POST",
                data: {
                    AddToCart: "AddToCart",
                    food_id: food_id,
                    food_name: food_name,
                    food_image: food_image,
                    food_price: food_price,
                    food_quantity: food_quantity,
                    food_TP: food_TP,
                    category_id: category_id,
                    user_session: user_session
                },
                success: function(Added_Food_Result) {

                    console.log(Added_Food_Result);

                    $("#msg_response").html(Added_Food_Result);
                    load_cart_food_number();
                    $("#ShowFood").modal("hide");
                    $(window).scrollTo(0, 0);

                }

            });

        });

        load_cart_food_number();

        function load_cart_food_number() {
            var user_session = $("#user_session_badge").val();
            $.ajax({

                url: "action.php",
                method: "GET",
                data: {
                    GetCartsItemCount: "GetCartsItemCount",
                    user_session: user_session
                },
                success: function(Get_Cart_Item_Count_Res) {

                    console.log(Get_Cart_Item_Count_Res);
                    $("#cart_item_number").html(Get_Cart_Item_Count_Res);
                }

            });

        }
    </script>

</body>

</html>