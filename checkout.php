<?php

require_once 'database/config.php';

if (isset($_GET['session'])) {

    $user_session = $_GET['session'] ? $_GET["session"] : '';
}
session_start();

$_SESSION['user_session'] = $user_session;
// get session
if (!isset($_SESSION['user_session'])) {

    // echo "<script>window.location.href='index';</script>";
    echo "<script>console.log('User session : {$_SESSION['user_session']} not set');</script>";
} else {

    $user_session = $_SESSION['user_session'];

    echo "<script>console.log('User session : {$user_session} set');</script>";
}

// getting orders
$total_price = 0;
$All_Items = "";
$items = [];

$fetch_items_sql = "SELECT CONCAT(fm_name, '(',fm_quantity,')') AS ItemQty, fm_total_price FROM carts WHERE user_session_id = '$user_session'";

$fetch_items_exe = mysqli_query($db_link, $fetch_items_sql);

while ($fetch_items_res = mysqli_fetch_array($fetch_items_exe)) {

    $total_price += $fetch_items_res["fm_total_price"];
    $items[] = $fetch_items_res["ItemQty"];
}

$All_Items = implode(', ', $items);


?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- favicon -->
    <link rel="shortcut icon" href="img/logo.jpg" type="image/x-icon">

    <title>Checkout | Fast Food</title>

    <!-- custom stylesheet -->
    <style>
        body {
            margin: 0;
        }

        /* body > #header{position:fixed;} */
        #header {
            width: 100%;
            margin-bottom: 100px;
            position:
                fixed;
            z-index: 9000;
            overflow: auto;
            background: #e6e6e6;
            text-align: center;
            padding: 10px 0;
            transition: all 0.15s linear;
        }

        #header.active {
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.4);
        }

        .header {

            margin-bottom: -35px;
            margin-top: 55.5555px;

        }

        .card {
            /* width: 100%; */
            height: 450px
        }

        .card-img-top {
            height: 220px;
        }

        .card a {
            background-color: #683a16;
            border-color: #683a16;
        }

        .btn-block {
            background-color: #683a16;
            border-color: #683a16;
            color: white;
        }

        .btn-block:hover {
            background-color: #683a16;
            border-color: #683a16;
            color: white;
        }

        .card a:hover {
            background-color: #f6f6f6;
            border-color: #683a16;
            color: #683a16;
        }

        .card-body {
            height: 217px;
            padding-bottom: 0rem;
        }

        .modal-header-danger {
            color: #fff;
            padding: 9px 15px;
            border-bottom: 1px solid #eee;
            background-color: #d9534f;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }

        div.form {
            display: block;
            text-align: center;
        }

        form {
            margin-left: 20%;
            margin-right: 20%;
            width: 60%;
        }
    </style>

</head>

<body class="bg-dark">

    <nav class="navbar navbar-expand-lg sticky-top navbar-light bg-light justify-content-between" id="header">
        <a class="navbar-brand mx-3">
            <img src="img/logo.jpg" alt="" width="30" height="24" class="d-inline-block align-text-top">
            Checkout | Fast Food
        </a>
        <!-- <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button> -->
        <a class="btn btn-outline-dark mx-3" href="cart?session=<?php echo $user_session; ?>">
            <input type="hidden" name="user_id" id="user_session_badge" value="<?php echo $user_session; ?>" />
            <i class="fa fa-shopping-cart"></i>
            Cart
            <span id="cart_item_number" class="badge bg-danger">0</span>
        </a>
        <!-- <a class="btn btn-outline-dark mx-3" href="login">Login</a> -->
    </nav>

    <div class="container">

        <div class="row m-3">

            <div class="col header">
                <h2 class="text-center text-light">My Order(s)</h2>
                <div id="msg_response" class="bg-light"></div>
            </div>

        </div>

        <div class="row align-items-center m-3">

            <div class="col-xs-6 mt-5 mb-3 px-4 pb-4">

                <div class="bg-light p-3 mb-3">

                    <h6 class="text-center lead">Item(s) : <b><?php echo $All_Items; ?></b> </h6>
                    <h6 class="lead text-center text-danger"><b>Free Delivery</b></h6>
                    <h6 class="text-center lead">Total amount payable : <b>Ghc <?php echo number_format($total_price, 2); ?></b> </h6>

                    <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" class="form-group form-submit">

                        <input type="hidden" name="items" value="<?php echo $All_Items; ?>" id="Items">
                        <input type="hidden" name="user_session_id" value="<?php echo $user_session; ?>" id="User_Session_Id">
                        <input type="hidden" name="total_price" value="<?php echo number_format($total_price, 2); ?>" id="Total_Price">

                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="client_name" id="ClientName" class="form-control" autofocus=true>
                        </div>

                        <div class="form-group">
                            <label>Contact</label>
                            <input type="number" name="client_contact" id="ClientContact" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>Delivery Address</label>
                            <textarea name="client_delivery_address" id="ClientDel_Address" cols="10" rows="3" class="form-control"></textarea>
                        </div>

                        <div class="form-group">
                            <label>Payment Method</label>
                            <select name="payment_method" id="PaymentMethod" class="form-control">
                                <option value="0">Select payment method</option>
                                <option value="Cash on delivery">Cash on delivery</option>
                                <option value="Mobile Money">Mobile Money</option>
                                <option value="Debit/Credit Card">Debit/Credit Card</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <input type="button" name="place_order" value="Place Order" id="btn_place_order" class="btn btn-success btn-block my-2">
                        </div>

                    </form>

                </div>

            </div>

        </div>

    </div>




    <!-- Jquery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>

    <!-- custom scripts -->
    <script>
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();
            if (scroll > 0) {
                $("#header").addClass("active");
            } else {
                $("#header").removeClass("active");
            }
        });

        $(document).ready(function() {

            load_cart_food_number();



            function load_cart_food_number() {
                var user_session = $("#user_session_badge").val();
                $.ajax({

                    url: "action.php",
                    method: "GET",
                    data: {
                        GetCartsItemCount: "GetCartsItemCount",
                        user_session: user_session
                    },
                    success: function(Get_Cart_Item_Count_Res) {

                        console.log(Get_Cart_Item_Count_Res);
                        $("#cart_item_number").html(Get_Cart_Item_Count_Res);

                        if (Get_Cart_Item_Count_Res <= 0) {

                            document.getElementById("Checkout_Button").style.visibility = 'hidden';

                        }
                    }

                });

            }

            // form reset / Reload
            function FormReload() {
                $(".form-submit")[0].reset();
            }

            // function ot reload page
            function ReloadPage() {
                location.reload(true);
            }


            // place order request
            $("#btn_place_order").click(function() {

                // get variables and values
                var Items = $("#Items").val();
                var user_session_id = $("#user_session_id").val();
                var Total_Price = $("#Total_Price").val();
                var ClientName = $("#ClientName").val();
                var ClientContact = $("#ClientContact").val();
                var ClientDel_Address = $("#ClientDel_Address").val();
                var PaymentMethod = $("#PaymentMethod").val();
                var user_session_id = $("#User_Session_Id").val();

                $.ajax({

                    url: 'action.php',
                    method: 'POST',
                    cache: false,
                    data: {
                        Place_Order: 'Place_Order',
                        Items: Items,
                        Total_Price: Total_Price,
                        ClientName: ClientName,
                        ClientContact: ClientContact,
                        ClientDel_Address: ClientDel_Address,
                        PaymentMethod: PaymentMethod,
                        user_session_id: user_session_id
                    },
                    success: function(GetOrderStatus) {

                        $("#msg_response").html(GetOrderStatus);
                        FormReload();
                        // function FormReload() {
                        //     $(".form-submit")[0].reset();
                        // }

                    }

                });

                // console.log( Items + " " + Total_Price);

            });

        });
    </script>

</body>

</html>