-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 12, 2021 at 10:35 PM
-- Server version: 5.7.33
-- PHP Version: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fast_food_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `cart_id` varchar(10) NOT NULL,
  `fm_id` varchar(10) NOT NULL,
  `fm_name` varchar(150) NOT NULL,
  `fm_image` varchar(150) NOT NULL,
  `fm_price` varchar(20) NOT NULL,
  `fm_quantity` varchar(10) NOT NULL,
  `fm_total_price` varchar(10) NOT NULL,
  `category_id` varchar(10) NOT NULL,
  `user_session_id` varchar(300) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated` timestamp NULL DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`cart_id`, `fm_id`, `fm_name`, `fm_image`, `fm_price`, `fm_quantity`, `fm_total_price`, `category_id`, `user_session_id`, `date_created`, `last_updated`, `timestamp`) VALUES
('0000000003', 'FM00003', 'Cinnamon Cake With Oreo Toppings', 'FM00003.jpg', '18.00', '1', '18.00', 'CAT0001', '9f55f33784cd1ed5b1bef3ec113bc42b', '2021-09-10 16:04:18', NULL, '2021-09-10 16:04:18'),
('0000000004', 'FM00001', 'Jollof Rice & Grilled Chicken', 'FM00001.jpg', '25.00', '2', '50.00', 'CAT0006', '9f55f33784cd1ed5b1bef3ec113bc42b', '2021-09-10 16:04:25', NULL, '2021-09-10 16:04:25'),
('0000000005', 'FM00004', 'French Red Wine', 'FM00004.jpg', '45.00', '1', '45.00', 'CAT0002', 'ec77771d518301ce9dd8c64b22b2db03', '2021-09-10 16:11:00', NULL, '2021-09-10 16:11:00'),
('0000000006', 'FM00013', 'Fufu With Goat Soup With Goat Meat, Fish, Pork', 'FM00013.jfif', '25.00', '1', '25.00', 'CAT0006', 'ec77771d518301ce9dd8c64b22b2db03', '2021-09-10 16:11:11', NULL, '2021-09-10 16:11:11'),
('0000000007', 'FM00016', 'Waakye With Fried Fish & Egg', 'FM00016.jpg', '15.00', '3', '45.00', 'CAT0006', 'ec77771d518301ce9dd8c64b22b2db03', '2021-09-10 16:11:16', NULL, '2021-09-10 16:11:16'),
('0000000008', 'FM00018', 'Chicken Pie', 'FM00018.jpg', '3.00', '2', '6.00', 'CAT0010', 'ec77771d518301ce9dd8c64b22b2db03', '2021-09-10 16:11:24', NULL, '2021-09-10 16:11:24'),
('0000000009', 'FM00014', 'Boiled Yam With Egg Stew With Pear', 'FM00014.jfif', '25.00', '1', '25.00', 'CAT0006', '9f55f33784cd1ed5b1bef3ec113bc42b', '2021-09-10 20:55:30', NULL, '2021-09-10 20:55:30'),
('0000000010', 'FM00003', 'Cinnamon Cake With Oreo Toppings', 'FM00003.jpg', '18.00', '1', '18.00', 'CAT0001', '55e942ad63a0842ea775e3ccf6d003c3', '2021-09-10 16:04:18', NULL, '2021-09-10 16:04:18'),
('0000000011', 'FM00004', 'French Red Wine', 'FM00004.jpg', '45.00', '1', '45.00', 'CAT0002', '55e942ad63a0842ea775e3ccf6d003c3', '2021-09-10 16:11:00', NULL, '2021-09-10 16:11:00');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) NOT NULL,
  `category_id` varchar(10) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated` datetime DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_id`, `category_name`, `date_created`, `last_updated`, `timestamp`) VALUES
(1, 'CAT0001', 'Pastries', '2021-07-07 23:42:56', NULL, '2021-07-07 23:42:56'),
(2, 'CAT0002', 'Wines', '2021-07-07 23:44:42', NULL, '2021-07-07 23:44:42'),
(3, 'CAT0003', 'Soft Drinks', '2021-07-07 23:45:33', NULL, '2021-07-07 23:45:33'),
(4, 'CAT0004', 'Liquors', '2021-07-07 23:46:36', NULL, '2021-07-07 23:46:36'),
(5, 'CAT0005', 'Water', '2021-07-07 23:47:22', NULL, '2021-07-07 23:47:22'),
(6, 'CAT0006', 'Ghanaian Dish', '2021-07-07 23:48:06', NULL, '2021-07-07 23:48:06'),
(7, 'CAT0007', 'Nigerian Dish', '2021-07-07 23:48:25', NULL, '2021-07-07 23:48:25'),
(8, 'CAT0008', 'Continental Dish', '2021-07-09 21:50:35', NULL, '2021-07-09 21:50:35'),
(9, 'CAT0009', 'Indian Dish', '2021-07-11 11:39:00', NULL, '2021-07-11 11:39:00'),
(10, 'CAT0010', 'Breakfast', '2021-07-15 11:22:30', NULL, '2021-07-15 11:22:30'),
(11, 'CAT0011', 'Fruits', '2021-07-19 07:51:12', NULL, '2021-07-19 07:51:12');

-- --------------------------------------------------------

--
-- Table structure for table `food_menus`
--

CREATE TABLE `food_menus` (
  `fm_id` varchar(20) NOT NULL,
  `fm_name` varchar(50) NOT NULL,
  `fm_description` text NOT NULL,
  `fm_image` varchar(100) NOT NULL,
  `fm_price` int(10) NOT NULL,
  `cat_id` varchar(20) NOT NULL,
  `fm_status` tinyint(1) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated` timestamp NULL DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `food_menus`
--

INSERT INTO `food_menus` (`fm_id`, `fm_name`, `fm_description`, `fm_image`, `fm_price`, `cat_id`, `fm_status`, `date_created`, `last_updated`, `timestamp`) VALUES
('FM00001', 'Jollof Rice &amp; Grilled Chicken', 'jollof rice &amp; grilled chicken', 'FM00001.jpg', 25, 'CAT0006', 0, '2021-07-15 10:50:25', NULL, '2021-07-15 10:50:25'),
('FM00002', 'Eba &amp; Egusi Soup With Assorted Meat', 'eba &amp; egusi soup with assorted meat', 'FM00002.jpg', 25, 'CAT0007', 0, '2021-07-15 10:51:54', NULL, '2021-07-15 10:51:54'),
('FM00003', 'Cinnamon Cake With Oreo Toppings', 'Cinnamon cake with oreo toppings', 'FM00003.jpg', 18, 'CAT0001', 0, '2021-07-15 10:52:39', NULL, '2021-07-15 10:52:39'),
('FM00004', 'French Red Wine', 'French red wine', 'FM00004.jpg', 45, 'CAT0002', 0, '2021-07-15 10:54:13', NULL, '2021-07-15 10:54:13'),
('FM00005', 'Belaqua (medium)', '', 'FM00005.png', 3, 'CAT0005', 0, '2021-07-15 11:14:34', NULL, '2021-07-15 11:14:34'),
('FM00006', 'Verna 500ml', '', 'FM00006.jpg', 3, 'CAT0005', 0, '2021-07-15 11:15:22', NULL, '2021-07-15 11:15:22'),
('FM00007', 'Voltic 500ml', '', 'FM00007.png', 3, 'CAT0005', 0, '2021-07-15 11:15:51', NULL, '2021-07-15 11:15:51'),
('FM00008', 'Pearl 350ml', '', 'FM00008.png', 3, 'CAT0005', 0, '2021-07-15 11:16:17', NULL, '2021-07-15 11:16:17'),
('FM00009', 'Soft Drinks', 'Coke, Sprite, Fanta, Tonic, Strawberry, Mint, Lemon', 'FM00009.jpg', 3, 'CAT0003', 0, '2021-07-15 11:18:49', NULL, '2021-07-15 11:18:49'),
('FM00010', 'Alvaro', 'Bottle, Plastic', 'FM00010.jpg', 4, 'CAT0003', 0, '2021-07-15 11:19:38', NULL, '2021-07-15 11:19:38'),
('FM00011', 'Polo', 'mix of coconut, floor, butter, nutmilk', 'FM00011.jfif', 2, 'CAT0001', 0, '2021-07-15 11:21:43', NULL, '2021-07-15 11:21:43'),
('FM00012', 'Hausa Coko With Kose', '', 'FM00012.jfif', 5, 'CAT0010', 0, '2021-07-15 11:23:04', NULL, '2021-07-15 11:23:04'),
('FM00013', 'Fufu With Goat Soup With Goat Meat, Fish, Pork', '', 'FM00013.jfif', 25, 'CAT0006', 0, '2021-07-15 11:24:31', NULL, '2021-07-15 11:24:31'),
('FM00014', 'Boiled Yam With Egg Stew With Pear', '', 'FM00014.jfif', 25, 'CAT0006', 0, '2021-07-15 11:25:33', NULL, '2021-07-15 11:25:33'),
('FM00015', 'Etor With Egg And Pear', '', 'FM00015.jpg', 20, 'CAT0006', 0, '2021-07-15 11:27:48', NULL, '2021-07-15 11:27:48'),
('FM00016', 'Waakye With Fried Fish &amp; Egg', '', 'FM00016.jpg', 15, 'CAT0006', 0, '2021-07-15 11:29:03', NULL, '2021-07-15 11:29:03'),
('FM00017', 'Amala With Ewudu With Assorted Meat', '', 'FM00017.jfif', 20, 'CAT0007', 0, '2021-07-15 11:31:21', NULL, '2021-07-15 11:31:21'),
('FM00018', 'Chicken Pie', 'Chicken Pie', 'FM00018.jpg', 3, 'CAT0010', 0, '2021-07-19 07:46:21', NULL, '2021-07-19 07:46:21'),
('FM00019', 'Fruit Salad (Apple, Watermelon, Pineapple, Banana)', '', 'FM00019.jfif', 5, 'CAT0011', 0, '2021-07-19 07:53:11', NULL, '2021-07-19 07:53:11');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` varchar(20) NOT NULL,
  `client_name` varchar(100) NOT NULL,
  `client_contact` varchar(20) NOT NULL,
  `client_delivery_address` varchar(300) NOT NULL,
  `payment_mode` varchar(20) NOT NULL,
  `items` text NOT NULL,
  `total_price` varchar(10) NOT NULL,
  `user_session_id` varchar(300) NOT NULL,
  `order_status` tinyint(1) NOT NULL,
  `ordered_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated` datetime DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `client_name`, `client_contact`, `client_delivery_address`, `payment_mode`, `items`, `total_price`, `user_session_id`, `order_status`, `ordered_date`, `last_updated`, `timestamp`) VALUES
('20210820001', 'PORTIA AMEYAW', '123456789', 'OGBODJO MADINA - ACCRA', 'Cash on delivery', 'Cinnamon Cake With Oreo Toppings(1), French Red Wine(1)', '63.00', '55e942ad63a0842ea775e3ccf6d003c3', 3, '2021-08-20 23:01:47', NULL, '2021-08-20 23:01:47'),
('20210910004', 'Abigail Owusu', '02547891123', 'Tema Community Center', 'Cash on delivery', 'French Red Wine(1), Fufu With Goat Soup With Goat Meat, Fish, Pork(1), Waakye With Fried Fish & Egg(3), Chicken Pie(2)', '121.00', 'ec77771d518301ce9dd8c64b22b2db03', 0, '2021-09-10 16:18:12', NULL, '2021-09-10 16:18:12'),
('20210910005', 'Shadrack Ohene Okai', '0234567890', 'Sowutuom Santa Maria Aunty Aku junction', 'Cash on delivery', 'Cinnamon Cake With Oreo Toppings(1), Jollof Rice & Grilled Chicken(2), Boiled Yam With Egg Stew With Pear(1)', '93.00', '9f55f33784cd1ed5b1bef3ec113bc42b', 0, '2021-09-10 20:56:30', NULL, '2021-09-10 20:56:30');

-- --------------------------------------------------------

--
-- Table structure for table `order_tracking`
--

CREATE TABLE `order_tracking` (
  `id` int(10) NOT NULL,
  `order_id` varchar(20) NOT NULL,
  `tracking_status` tinyint(1) NOT NULL,
  `tracking_comments` text NOT NULL,
  `tracking_date` date NOT NULL,
  `tracking_time` time NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_tracking`
--

INSERT INTO `order_tracking` (`id`, `order_id`, `tracking_status`, `tracking_comments`, `tracking_date`, `tracking_time`, `timestamp`) VALUES
(1, '20210820001', 0, 'Order placed successfully', '2021-08-20', '23:01:47', '2021-08-20 23:01:47'),
(2, '20210820002', 0, 'Order placed successfully', '2021-08-20', '23:04:04', '2021-08-20 23:04:04'),
(3, '20210820003', 0, 'Order placed successfully', '2021-08-20', '23:06:04', '2021-08-20 23:06:04'),
(5, '20210820003', 1, 'food is ready and is being packed', '2021-09-03', '15:39:37', '2021-09-03 15:39:37'),
(6, '20210820003', 2, 'food being delivered at 1pm', '2021-09-03', '15:48:10', '2021-09-03 15:48:10'),
(7, '20210820001', 1, 'food is ready and is being packed', '2021-09-03', '15:59:58', '2021-09-03 15:59:58'),
(8, '20210820001', 2, 'food being delivered at 1pm', '2021-09-03', '16:01:42', '2021-09-03 16:01:42'),
(9, '20210820001', 3, 'order received by client @ 3pm', '2021-09-03', '16:14:18', '2021-09-03 16:14:18'),
(10, '20210820003', 3, 'order received by client @ 3pm', '2021-09-03', '16:19:37', '2021-09-03 16:19:37'),
(11, '20210910004', 0, 'Order placed successfully', '2021-09-10', '16:18:12', '2021-09-10 16:18:12'),
(12, '20210910005', 0, 'Order placed successfully', '2021-09-10', '20:56:30', '2021-09-10 20:56:30');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(400) NOT NULL,
  `role` varchar(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `timestamp` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`, `created_at`, `timestamp`) VALUES
(1, 'admin@fastfood.com', '23d42f5f3f66498b2c8ff4c20b8c5ac826e47146', 'Admin', '2021-07-07 18:16:44', '2021-07-07 18:16:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `food_menus`
--
ALTER TABLE `food_menus`
  ADD PRIMARY KEY (`fm_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_tracking`
--
ALTER TABLE `order_tracking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `order_tracking`
--
ALTER TABLE `order_tracking`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
