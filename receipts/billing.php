<?php

require_once("../database/config.php");

if (isset($_GET["utm"])) {

    $x_variable = $_GET["utm"] ? $_GET["utm"] : "";

    $get_client_info_sql = "SELECT * FROM orders WHERE order_id = '$x_variable'";
    $get_client_info_exe = mysqli_query($db_link, $get_client_info_sql);

    while ($g_res_row = mysqli_fetch_array($get_client_info_exe)) {

        $client_name = $g_res_row["client_name"];
        $client_contact = $g_res_row["client_contact"];
        $client_address = $g_res_row["client_delivery_address"];
        $ordered_date = $g_res_row["ordered_date"];
        $client_session_id = $g_res_row["user_session_id"];
        $payment_method = $g_res_row["payment_mode"];
        $total_price = $g_res_row["total_price"];
    }
}

require("../fpdf/fpdf.php");

$pdf = new FPDF();

$pdf->AddPage();

// set font tp times, bold, 16pt
$pdf->SetFont('times', 'B', 16);

// Image
// $pdf->Image('../img/logo.jpg', 10, 10, 15, 0);

// cell(width, height, text, border, end line, align)
$pdf->Cell(130, 5, 'Campus Fast Food', 0, 0);
$pdf->SetFont('times', '', 12);
$pdf->Cell(60, 5, $ordered_date, 0, 1, 'C');
$pdf->SetFont('times', '', 12);
$pdf->Cell(120, 5, 'Pentecost University', 0, 1);
$pdf->Cell(120, 5, 'campusfastfood@gmail.com', 0, 1);
$pdf->Cell(90, 5, '+233 20 884 7094 / +233 20 727 5541', 0, 1);

$pdf->Cell(190, 10, '', 0, 1);
$pdf->SetFont('times', 'B', 16);
$pdf->Cell(190, 15, 'ORDER #' . $x_variable, 0, 1, 'C');
$pdf->Cell(190, 10, '', 0, 1);

$pdf->SetFont('times', '', 14);
$pdf->Cell(110, 10, 'Bill To', 0, 0);
$pdf->Cell(80, 10, 'Payment Method', 0, 1);
$pdf->SetFont('times', 'B', 14);
$pdf->Cell(15, 7, '', 0, 0);
$pdf->Cell(95, 7, $client_name, 0, 0);
$pdf->Cell(70, 7, $payment_method, 0, 1);
$pdf->Cell(15, 7, '', 0, 0);
$pdf->Cell(165, 7, $client_address, 0, 1);
$pdf->Cell(15, 7, '', 0, 0);
$pdf->Cell(165, 7, $client_contact, 0, 1);
$pdf->Cell(190, 10, '', 0, 1);

$pdf->SetFont('times', '', 14);
$pdf->Cell(110, 7, 'Description', 1, 0, 'C');
$pdf->Cell(20, 7, 'Quantity', 1, 0, 'C');
$pdf->Cell(30, 7, 'Unit Price', 1, 0, 'C');
$pdf->Cell(30, 7, 'Total', 1, 1, 'C');

if ($client_session_id) {

    $get_all_items_sql = "SELECT * FROM carts WHERE user_session_id = '$client_session_id'";

    $get_all_items_exe = mysqli_query($db_link, $get_all_items_sql);

    while ($get_items_res = mysqli_fetch_array($get_all_items_exe)) {

        $item_name = $get_items_res["fm_name"];
        $item_quantity = $get_items_res["fm_quantity"];
        $item_unit_price = $get_items_res["fm_price"];
        $item_total_price = $get_items_res["fm_total_price"];

        $pdf->Cell(110, 7, $item_name, 1, 0);
        $pdf->Cell(20, 7, $item_quantity, 1, 0, 'C');
        $pdf->Cell(30, 7, $item_unit_price, 1, 0, 'C');
        $pdf->Cell(30, 7, $item_total_price, 1, 1, 'C');
    }
}

$pdf->Cell(160, 7, 'Grand Total', 1, 0, 'R');
$pdf->SetFont('times', 'B', 14);
$pdf->Cell(30, 7, "GHc" . $total_price, 1, 1, 'C');

$pdf->Cell(190, 10, '', 0, 1);

$pdf->Ln(10);

$pdf->Cell(190, 10, '', 0, 1);
$pdf->SetFont('times', 'B', 16);
$pdf->Cell(190, 10, 'Thank you for dining with us!!!', 0, 1, 'C');

$pdf->Output();
