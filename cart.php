<?php

require_once 'database/config.php';

if (isset($_GET['session'])) {

    $user_session = $_GET['session'] ? $_GET["session"] : '';
}
session_start();

$_SESSION['user_session'] = $user_session;
// get session
if (!isset($_SESSION['user_session'])) {

    // echo "<script>window.location.href='index';</script>";
    echo "<script>console.log('User session : {$_SESSION['user_session']} not set');</script>";
} else {

    $user_session = $_SESSION['user_session'];

    echo "<script>console.log('User session : {$user_session} set');</script>";
}


?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- favicon -->
    <link rel="shortcut icon" href="img/logo.jpg" type="image/x-icon">

    <title>Cart | Fast Food</title>

    <!-- custom stylesheet -->
    <style>
        body {
            margin: 0;
        }

        /* body > #header{position:fixed;} */
        #header {
            width: 100%;
            margin-bottom: 100px;
            position:
                fixed;
            z-index: 9000;
            overflow: auto;
            background: #e6e6e6;
            text-align: center;
            padding: 10px 0;
            transition: all 0.15s linear;
        }

        #header.active {
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.4);
        }

        .header {

            margin-bottom: -35px;
            margin-top: 55.5555px;

        }

        .card {
            /* width: 100%; */
            height: 450px
        }

        .card-img-top {
            height: 220px;
        }

        .card a {
            background-color: #683a16;
            border-color: #683a16;
        }

        .btn-block {
            background-color: #683a16;
            border-color: #683a16;
            color: white;
        }

        .btn-block:hover {
            background-color: #683a16;
            border-color: #683a16;
            color: white;
        }

        .card a:hover {
            background-color: #f6f6f6;
            border-color: #683a16;
            color: #683a16;
        }

        .card-body {
            height: 217px;
            padding-bottom: 0rem;
        }

        .modal-header-danger {
            color: #fff;
            padding: 9px 15px;
            border-bottom: 1px solid #eee;
            background-color: #d9534f;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }
    </style>

</head>

<body class="bg-dark">

    <nav class="navbar navbar-expand-lg sticky-top navbar-light bg-light justify-content-between" id="header">
        <a class="navbar-brand mx-3">
            <img src="img/logo.jpg" alt="" width="30" height="24" class="d-inline-block align-text-top">
            Cart | Fast Food
        </a>
        <!-- <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button> -->
        <a class="btn btn-outline-dark mx-3">
            <input type="hidden" name="user_id" id="user_session_badge" value="<?php echo $user_session; ?>" />
            <i class="fa fa-shopping-cart"></i>
            Cart
            <span id="cart_item_number" class="badge bg-danger">0</span>
        </a>
        <!-- <a class="btn btn-outline-dark mx-3" href="login">Login</a> -->
    </nav>

    <div class="container">

        <div class="row m-3">

            <div class="col header">
                <h2 class="text-center text-light">My Cart</h2>
                <div id="msg_response" class=""></div>
            </div>

        </div>

        <div class="row align-items-center m-3">

            <div class="col mt-5 mb-3">

                <table class="table table-striped bg-light">

                    <thead>

                        <tr>
                            <th scope="col" class="text-center">Image</th>
                            <th scope="col" class="text-left">Name</th>
                            <th scope="col" class="text-center">Price</th>
                            <th scope="col" class="text-center">Quantity</th>
                            <th scope="col" class="text-center">Total Price</th>
                            <th scope="col" class="text-center">
                                <a class="btn btn-sm btn-danger" data-bs-toggle="modal" data-bs-target="#DeleteAllItems" data-user_inSession="<?php echo $user_session ?>">Empty Cart</a>
                            </th>
                        </tr>

                    </thead>

                    <tbody>

                        <!-- fetch user items in card using user session -->
                        <?php
                        $subtotal = 0;
                        $fetch_user_cart_items = "SELECT * FROM carts WHERE user_session_id = '$user_session'";

                        $exe_query = mysqli_query($db_link, $fetch_user_cart_items);

                        if ($exe_query) {

                            if (mysqli_num_rows($exe_query) <= 0) {

                        ?>

                                <tr>

                                    <td class="table-danger text-center" colspan="6">No data entries</td>

                                </tr>

                                <?php

                            } else {

                                while ($cart_item_row = mysqli_fetch_array($exe_query)) {

                                    $food_id = $cart_item_row["fm_id"];
                                    $cart_id = $cart_item_row["cart_id"];
                                    $food_image = $cart_item_row["fm_image"];
                                    $food_name = $cart_item_row["fm_name"];
                                    $food_price = $cart_item_row["fm_price"];
                                    $food_quantity = $cart_item_row["fm_quantity"];
                                    $food_TP = $cart_item_row["fm_total_price"];

                                ?>

                                    <tr>


                                        <td><img src="img/<?php echo $food_image; ?>" alt="" width="50" height="50"></td>
                                        <td><?php echo $food_name; ?></td>
                                        <td class="text-center"><?php echo $food_price; ?></td>
                                        <td class="text-center"><?php echo $food_quantity; ?></td>
                                        <td class="text-center"><?php echo $food_TP; ?></td>
                                        <td class="text-center">
                                            <a class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#RemoveItem" data-rmv_item_id="<?php echo $cart_id; ?>" data-user_id="<?php echo $user_session; ?>" data-rmv_item_name="<?php echo $food_name; ?>">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>

                                        <?php $subtotal += $food_TP; ?>

                                    </tr>

                                <?php

                                }

                                ?>

                    <tfoot>

                        <tr>

                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <td class="text-right">Sub-Total</td>
                            <td class="text-center"><?php echo number_format($subtotal, 2); ?></td>

                        </tr>

                        <tr>

                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th class="text-right">Total</th>
                            <th class="text-center"><?php echo number_format($subtotal, 2); ?></th>

                        </tr>

                    </tfoot>

            <?php

                            }
                        }

            ?>


            </tbody>


                </table>

                <div class="col mt-5 mb-3">

                    <div class="row">

                        <div class="col-sm-12 col-md-6 text-center">

                            <a href="index?session=<?php echo $user_session; ?>" class="btn btn-block">

                                <i class="fa fa-shopping-cart"></i>

                                Continue shopping

                            </a>

                        </div>

                        <div class="col-sm-12 col-md-6 text-center">

                            <a href="checkout?session=<?php echo $user_session; ?>" class="btn btn-block bg-success text-uppercase" id="Checkout_Button">Checkout</a>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- Delete specific item modal -->
    <div class="modal fade" id="RemoveItem" tabindex="-1" aria-labelledby="RemoveItemLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <!-- form to add item to cart -->
                <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" class="form-submit" method="post">
                    <div class="modal-header modal-header-danger">
                        <h4 class="modal-title" id="Name_Label">Cart Item Removal</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <h4 id="p_note"></h4>
                            <input type="hidden" name="cart_id" id="Cart_Id">
                            <input type="hidden" name="user_session" id="User_Session">

                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-block btn-danger" name="delete_item" id="btn_cart_id" value="Delete Item">
                    </div>

                </form>
            </div>
        </div>
    </div>

    <!-- Delete all items message popup -->
    <div class="modal fade" id="DeleteAllItems" tabindex="-1" aria-labelledby="DeleteAllItemsLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <!-- form to add item to cart -->
                <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" class="form-submit" method="post">
                    <div class="modal-header modal-header-danger">
                        <h4 class="modal-title" id="Name_Label">Removal of all Items in Cart</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <input type="hidden" name="" value="" id="User_InSession_ID">
                            <h4 id="p_note">Are you sure to clear all items in your cart?</h4>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-block btn-danger" name="delete_item" id="btn_items" value="Delete Items">
                    </div>

                </form>
            </div>
        </div>
    </div>


    <!-- Jquery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>

    <!-- custom scripts -->
    <script>
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();
            if (scroll > 0) {
                $("#header").addClass("active");
            } else {
                $("#header").removeClass("active");
            }
        });

        $(document).ready(function() {

            load_cart_food_number();



            function load_cart_food_number() {
                var user_session = $("#user_session_badge").val();
                $.ajax({

                    url: "action.php",
                    method: "GET",
                    data: {
                        GetCartsItemCount: "GetCartsItemCount",
                        user_session: user_session
                    },
                    success: function(Get_Cart_Item_Count_Res) {

                        console.log(Get_Cart_Item_Count_Res);
                        $("#cart_item_number").html(Get_Cart_Item_Count_Res);

                        if (Get_Cart_Item_Count_Res <= 0) {

                            document.getElementById("Checkout_Button").style.visibility = 'hidden';

                        }
                    }

                });

            }

            // form reset / Reload
            function FormReload() {
                $(".form-submit")[0].reset();
            }

            // function ot reload page
            function ReloadPage() {
                location.reload(true);
            }

            // delete specific item from cart using ajax request and php by clicking on the trash icon on the specific item
            $("#RemoveItem").on("shown.bs.modal", function(event) {

                FormReload();
                // get the trash icon btn event to pass the different variable in the popup
                var str = $(event.relatedTarget);

                // getting values from specific item row and passing it in the variables
                var rmv_item_id = str.data('rmv_item_id');
                var user_id = str.data('user_id');
                var rmv_item_name = str.data('rmv_item_name');

                // getting the popup form by id to display the above values in it
                var modal = $(this);

                // assigning variables to corresponding field in the popup form
                modal.find("#p_note").html("Are you sure on removing " + rmv_item_name + " ?");
                modal.find("#Cart_Id").val(rmv_item_id);
                modal.find("#User_session").val(user_id);

            });

            // get user in session id to all items put in cart
            $("#DeleteAllItems").on("shown.bs.modal", function(event) {
                // get the button which  triggers the modal to show by getting its elements
                var str = $(event.relatedTarget);

                var user_InSession_ID = str.data("user_insession");

                var modal = $(this);

                modal.find("#user_inSession_ID").val(user_InSession_ID);

            });


            // when btn_cart_id is clicked to delete the specific item
            $("#btn_cart_id").click(function() {

                var deleteModal = $(this).closest(".form-submit");

                var cart_id = deleteModal.find("#Cart_Id").val();
                var user_session = deleteModal.find("#User_Session").val();
                $.ajax({

                    url: "action.php",
                    method: "POST",
                    cache: false,
                    data: {
                        RemoveItem: "RemoveItem",
                        cart_id: cart_id,
                        user_session: user_session
                    },
                    success: function(RemoveItemResult) {

                        $("#msg_response").html(RemoveItemResult);
                        ReloadPage();
                        load_cart_food_number();
                        $("#RemoveItem").modal("hide");
                        // $(window).scrollTo(0, 0);
                    }

                });

            });

            //delete all items in cart
            $("#btn_items").click(function() {

                var DelItems = $(this).closest(".form-submit");

                var user_insession_ID = DelItems.find("#User_InSession_ID").val();

                // console.log(user_insession_ID);

                $.ajax({

                    url: "action.php",
                    method: "POST",
                    data: {
                        DeleteAllItems: 'DeleteAllItems',
                        user_insession_ID: user_insession_ID
                    },
                    success: function(DeleteAllItemsResult) {

                        $("#msg_response").html(DeleteAllItemsResult);
                        ReloadPage();
                        load_cart_food_number();
                        $("#DeleteAllItems").modal("hide");
                    }

                });

            });

        });
    </script>

</body>

</html>