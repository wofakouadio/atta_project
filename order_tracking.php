<?php
session_start();
require_once 'database/config.php';

// getting session id
$y = md5(session_id());

$user_session = $_SESSION["user_session "] = $y;

echo "<script>
        console.log('User session : {$_SESSION["user_session "]}');
        </script>";

?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- favicon -->
    <link rel="shortcut icon" href="img/logo.jpg" type="image/x-icon">

    <title>Order Tracking | Fast Food</title>

    <!-- custom stylesheet -->
    <style>
        body {
            margin: 0;
        }

        /* body > #header{position:fixed;} */
        #header {
            width: 100%;
            margin-bottom: 100px;
            position:
                fixed;
            z-index: 9000;
            overflow: auto;
            background: #e6e6e6;
            text-align: center;
            padding: 10px 0;
            transition: all 0.15s linear;
        }

        #header.active {
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.4);
        }

        .header {

            margin-bottom: -35px;
            margin-top: 55.5555px;

        }

        .card {
            /* width: 100%; */
            height: 450px
        }

        .card-img-top {
            height: 220px;
        }

        .card a {
            background-color: #683a16;
            border-color: #683a16;
        }

        .btn-block {
            background-color: #683a16;
            border-color: #683a16;
            color: white;
        }

        .btn-block:hover {
            background-color: #683a16;
            border-color: #683a16;
            color: white;
        }

        .card a:hover {
            background-color: #f6f6f6;
            border-color: #683a16;
            color: #683a16;
        }

        .card-body {
            height: 217px;
            padding-bottom: 0rem;
        }

        input[type="text"] {

            font-size: 23pt;

        }

        input[type="button"] {

            font-size: 23pt;

        }
    </style>

</head>

<body class="bg-dark">

    <nav class="navbar navbar-expand-lg sticky-top navbar-light bg-light justify-content-between" id="header">
        <a class="navbar-brand mx-3" href=".">
            <img src="img/logo.jpg" alt="" width="30" height="24" class="d-inline-block align-text-top">
            Order Tracking | Fast Food
        </a>
        <!-- <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button> -->
        <!-- <a class="btn btn-outline-dark mx-3" href="cart?session=<?php echo $user_session; ?>">
            <input type="hidden" name="user_id" id="user_session_badge" value="<?php echo $user_session; ?>" />
            <i class="fa fa-shopping-cart"></i>
            Cart
            <span id="cart_item_number" class="badge bg-danger">0</span>
        </a> -->
        <a class="btn btn-outline-dark mx-3" href="/">Order Now</a>
    </nav>

    <div class="container">

        <div class="row m-3">

            <div class="col header">
                <h2 class="text-center text-light">Order Tracking</h2>
            </div>

        </div>

        <div class="row align-items-center m-3">

            <div class="col-md">

                <div class="jumbotron bg-light p-5 my-4 display-6">

                    <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" class="form-group" method="post" id="form-submit">

                        <div class="form-group">
                            <label for="">Track your order using order ID</label>
                            <input type="number" name="order_id" id="order_tracking" class="form-control mt-3" autofocus>
                        </div>

                        <div class="form-group">
                            <!-- <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> -->
                            <input type="button" class="btn btn-block add_food my-3" name="" id="btn_track" value="Track">
                        </div>

                    </form>

                    <div id="msg_response" class=""></div>

                </div>

            </div>

        </div>

    </div>


    <!-- Jquery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>

    <!-- custom scripts -->
    <script>
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();
            if (scroll > 0) {
                $("#header").addClass("active");
            } else {
                $("#header").removeClass("active");
            }
        });

        // form reset
        function FormReload() {
            $("#form-submit")[0].reset();
        }

        $('#btn_track').click(function() {

            var order_id = $('#order_tracking').val();

            if (order_id != "") {

                $.ajax({

                    url: 'action',
                    method: 'POST',
                    cache: false,
                    data: {
                        tracking_id: 'tracking_id',
                        order_id: order_id
                    },
                    beforeSend: function() {
                        $('#msg_response').html('<div class="alert alert-info">Searching...</div>');
                    },
                    success: function(TrackingResult) {
                        setTimeout(function() {
                            $('#msg_response').fadeIn('slow').html(TrackingResult);
                            FormReload();
                            document.getElementById('order_tracking').focus();
                        }, 3500)
                    }

                });


            } else {

                alert('Please Order ID is empty');
                document.getElementById('order_tracking').focus();

            }

        });
    </script>

</body>

</html>