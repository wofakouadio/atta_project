<?php
// start session
// session_start();

// include database connection
require_once "database/config.php";





// To food item to cart
// when add to cart is set
// ---------------------------------------------------------------------
if (isset($_POST["AddToCart"])) {

    if (isset($_POST["AddToCart"]) == "AddToCart") {

        // declaring and assigning values to variables
        $food_id = $_POST["food_id"];
        $food_name = $_POST["food_name"];
        $food_image = $_POST["food_image"];
        $food_price = $_POST["food_price"];
        $food_quantity = $_POST["food_quantity"];
        $food_TP = $_POST["food_TP"];
        $category_id = $_POST["category_id"];
        $user_session = $_POST["user_session"];

        // function autogenerate cart id
        function CartID($db_link)
        {

            $fetch_carts_sql = "SELECT * FROM carts";

            $fetch_carts_exe = mysqli_query($db_link, $fetch_carts_sql);

            if ($fetch_carts_exe) {

                if (mysqli_num_rows($fetch_carts_exe) == 0) {

                    $count_from = 1;
                    $cart_ID = sprintf('%010d', $count_from);
                }

                if (mysqli_num_rows($fetch_carts_exe) > 0) {

                    $fetch_lastCart_sql = "SELECT cart_id FROM carts ORDER BY cart_id DESC LIMIT 1";
                    $fetch_lastCart_exe = mysqli_query($db_link, $fetch_lastCart_sql);
                    $fetch_lastCart_res = mysqli_fetch_array($fetch_lastCart_exe);
                    $fetch_lastCart = $fetch_lastCart_res["cart_id"];
                    $explode = substr($fetch_lastCart, -10);
                    $count_from = "$explode" + 1;
                    $cart_ID = sprintf('%010d', $count_from);
                }
            }

            return $cart_ID;
        }

        $card_id = CartID($db_link);

        // echo "<div class='alert alert-success alert-dismissible mt-2' id='MsgAlert'>Cart ID = ".CartID($db_link)."</div>";

        // insert food item to cart through this session
        $ins_food_item = "INSERT INTO carts (cart_id, fm_id, fm_name, fm_image, fm_price, fm_quantity, fm_total_price, category_id, user_session_id) VALUES ('$card_id', '$food_id', '$food_name', '$food_image', '$food_price', '$food_quantity', '$food_TP', '$category_id', '$user_session')";

        $exe_food_item = mysqli_query($db_link, $ins_food_item);

        if ($exe_food_item) {

            echo "<div class='alert alert-success alert-dismissible mt-2' id='MsgAlert'>" . $food_name . " added to cart</div>";
        } else {

            echo "<div class='alert alert-warning alert-dismissible mt-2' id='MsgAlert'>Unable to add " . $food_name . " to cart " . mysqli_error($db_link) . "</div>";
        }
    }
}

// load cart count item inserted based on user session
if (isset($_GET["GetCartsItemCount"]) && isset($_GET["user_session"])) {

    if ((isset($_GET["GetCartsItemCount"]) == "GetCartsItemCount")) {

        // if(isset($_GET["user_session"])){

        $user_session = $_GET["user_session"] ? $_GET["user_session"] : "";

        $fetch_cart_count_sql = "SELECT COUNT(*) AS TOTAL_ITEM_COUNT FROM carts WHERE user_session_id = '$user_session'";

        $fetch_cart_count_exe = mysqli_query($db_link, $fetch_cart_count_sql);

        $fetch_cart_count_result = mysqli_fetch_assoc($fetch_cart_count_exe);

        if ($fetch_cart_count_result["TOTAL_ITEM_COUNT"] <= 0) {

            echo sprintf("%01d", $fetch_cart_count_result["TOTAL_ITEM_COUNT"]);
        } else {

            echo sprintf("%01d", $fetch_cart_count_result["TOTAL_ITEM_COUNT"]);
        }

        // }

    }
}

// remove specific item from cart
if (isset($_POST["RemoveItem"])) {

    if (isset($_POST["RemoveItem"]) == "RemoveItem") {

        $cart_id = $_POST["cart_id"];
        $user_session = $_POST["user_session"];

        $del_item_sql = "DELETE FROM carts WHERE cart_id = '$cart_id'";

        $del_item_exe = mysqli_query($db_link, $del_item_sql);

        if ($del_item_exe) {

            echo "<script>window.alert('Item removed from cart successful');</script>";
        } else {

            echo "<script>window.alert('Item removal failed from cart successful')</script>";
        }
    }
}


// delete all items from cart using user session id
if (isset($_POST["DeleteAllItems"])) {

    if (isset($_POST["DeleteAllItems"]) == "DeleteAllItems") {

        $user_insession_ID = $_POST["user_insession_ID"];

        $del_items_sql = "DELETE FROM carts WHERE user_session_id = '$user_insession_ID'";

        $del_items_exe = mysqli_query($db_link, $del_items_sql);

        if ($del_items_exe) {

            echo "<script>window.alert('Items removed from cart successful');</script>";
        } else {

            echo "<script>window.alert('Items removal failed from cart successful')</script>";
        }
    }
}

// sms api key = JndcmWEykss8io0V8IQtPuURb
// sms api k
// sms sender ID = PU-FastFood / WOFAKOUADIO

// place order for items
if (isset($_POST["Place_Order"])) {

    // session_start();

    if (isset($_POST["Place_Order"]) == "Place_Order") {

        // function to create order id
        function OrderID($db_link)
        {

            $get_all_orders_sql = "SELECT * FROM orders";

            $get_all_orders_exe = mysqli_query($db_link, $get_all_orders_sql);

            if ($get_all_orders_exe) {

                if (mysqli_num_rows($get_all_orders_exe) == 0) {

                    $count_from = 1;
                    $id = date("Ymd") . sprintf("%03d", $count_from);
                }

                if (mysqli_num_rows($get_all_orders_exe) > 0) {

                    $get_last_order_sql = "SELECT order_id FROM orders ORDER BY order_id DESC LIMIT 1";
                    $get_last_order_exe = mysqli_query($db_link, $get_last_order_sql);
                    $get_last_order_res = mysqli_fetch_array($get_last_order_exe);
                    $get_last_order = $get_last_order_res["order_id"];
                    $explode = substr($get_last_order, -3);
                    $count_from = "$explode" + 1;
                    $id = date("Ymd") . sprintf("%03d", $count_from);
                }
            }

            return $id;
        }

        $Items = $_POST["Items"];
        $Total_Price = $_POST["Total_Price"];
        $ClientName = ucwords($_POST["ClientName"]);
        $ClientContact = $_POST["ClientContact"];
        $ClientDel_Address = $_POST["ClientDel_Address"];
        $PaymentMethod = $_POST["PaymentMethod"];
        $user_session_id = $_POST["user_session_id"];
        $order_id = OrderID($db_link);


        $ins_order_sql = "INSERT INTO orders(order_id, client_name, client_contact, client_delivery_address, payment_mode, items, total_price, user_session_id, order_status) VALUES('$order_id', '$ClientName', '$ClientContact', '$ClientDel_Address', '$PaymentMethod', '$Items', '$Total_Price', '$user_session_id', 0)";

        $ins_order_tracking_sql = "INSERT INTO order_tracking(order_id, tracking_status, tracking_comments, tracking_date, tracking_time) VALUES('$order_id', 0, 'Order placed successfully' ,NOW(), NOW())";

        $ins_orders_exe = mysqli_query($db_link, $ins_order_sql);

        $ins_order_tracking_exe = mysqli_query($db_link, $ins_order_tracking_sql);

        if ($ins_orders_exe && $ins_order_tracking_exe) {

            echo "<div class='text-center mt-2 mb-4'>
                    <h2 class='display-4  text-success'>Thank You!!!</h2>
                    <h3>Your order has been successful</h3>
                    <h3>Order ID : $order_id</h3>
                    <h4 class='bg-warning rounded p-2'> Items purchased : $Items</h4>
                    <h5>Name : $ClientName</h5>
                    <h5>Contact : $ClientContact</h5>
                    <h5>Delivery Address : $ClientDel_Address</h5>
                    <h5>Total Amount to be paid : GHc $Total_Price</h5>
                    <h5>Payment Method : $PaymentMethod</h5>
                    <a href='reset_session' class='btn btn-primary'>Continue shopping</a>
                </div>";
        } else {

            echo "<div class='text-center'>
                    <h2 class='display-4 mt-2 mb-2 text-danger'>Sorry, Order failed to be placed! Try again later. " . mysqli_error($db_link) . "</h2>
                </div>";
        }
    }
}


// track order order based on order id
if (isset($_POST["tracking_id"])) {

    if (isset($_POST["tracking_id"]) == "tracking_id") {

        $order_id = $_POST["order_id"];

        $fetch_id_trackings_sql = "SELECT * FROM order_tracking ot JOIN orders o ON ot.order_id = o.order_id WHERE ot.order_id = o.order_id && ot.order_id = '$order_id' ORDER BY ot.order_id DESC";
        $fetch_id_trackings_exe = mysqli_query($db_link, $fetch_id_trackings_sql);

        if ($fetch_id_trackings_exe) {
            $res = "<h6 class='display-4 text-center'>Order ID: #$order_id</h6>";
            $res .= "<table class='table table-sm table-striped'>";
            $res .= "<thead>";
            $res .= "<tr>";
            $res .= "<th class='text-center'>Date</th>";
            $res .= "<th class='text-center'>Time</th>";
            $res .= "<th class='text-center'>Status</th>";
            $res .= "<th class='text-center'>Comments</th>";
            $res .= "</tr>";
            $res .= "</thead>";
            $res .= "<tbody>";

            if (mysqli_num_rows($fetch_id_trackings_exe) <= 0) {
                $res .= "<tr>";
                $res .= "<td colspan='4' class='text-center'>Invalid Order ID</td>";
                $res .= "</tr>";
            } else {

                while ($fetch_id_trackings_res = mysqli_fetch_array($fetch_id_trackings_exe)) {

                    $t_order_id = $fetch_id_trackings_res["order_id"];
                    $t_status = $fetch_id_trackings_res["tracking_status"];
                    $t_tracking_comments = $fetch_id_trackings_res["tracking_comments"];
                    $t_tracking_date = $fetch_id_trackings_res["tracking_date"];
                    $t_tracking_time = $fetch_id_trackings_res["tracking_time"];

                    if ($t_status == 0) {

                        $t_status = "<span class='badge bg-danger text-center text-uppercase'>placed</span>";
                    } else if ($t_status == 1) {

                        $t_status = "<span class='badge bg-info text-center text-uppercase'>ready</span>";
                    } else if ($t_status == 2) {

                        $t_status = "<span class='badge bg-warning text-center text-uppercase'>in delivery</span>";
                    } else if ($t_status == 3) {

                        $t_status = "<span class='badge bg-success text-center text-uppercase'>received</span>";
                    } else {
                        $t_status = "<span class='badge bg-success text-center text-uppercase'>picked</span>";
                    }

                    $res .= "<tr>";
                    $res .= "<td class='text-center'>" . $t_tracking_date . "</td>";
                    $res .= "<td class='text-center'>" . $t_tracking_time . "</td>";
                    $res .= "<td class='text-center'>" . $t_status . "</td>";
                    $res .= "<td>" . $t_tracking_comments . "</td>";
                    $res .= "</tr>";
                }
            }
            $res .= "</tbody>";
            $res .= "</table>";
        }

        echo $res;
    }
}
