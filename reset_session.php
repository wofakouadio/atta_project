<?php

session_start();

if (session_destroy()) {
    setcookie("PHPSESSID", "", 1);
    header("Location:index");
}
