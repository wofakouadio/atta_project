<?php 
    session_start();
    // include 'includes/session.php';
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" />
    <!-- favicon -->
    <link rel="shortcut icon" href="img/logo.jpg" type="image/x-icon">

    <title>Fast Food | Admin Login</title>

    <!-- custom stylesheet -->
    <style>
        body {
            margin: 0;
        }

        input,
        .form-control {
            margin-top: 20px;
            margin-bottom: 20px;
            height: 50px;
            font-size: .8rem;
            border-radius: 10rem;
            padding: 1.5rem 1rem;
        }

        .btn-user {
            font-size: .8rem;
            width: 100%;
            border-radius: 10rem;
            padding: .75rem 1rem;
            color: black;
            background-color: #f6f6f6;
            border-color: #683a16;
        }
        .btn-user:hover{
            background-color: #683a16;
        }
        
        .btn-login-image{
            border-top-left-radius: 100rem;
        }
    </style>

</head>

<body class="bg-dark">

    <div class="container">

        <div class="row justify-content-center">

            <div class="col-md-9 col-lg-12 col-xl-10">

                <div class="card shadow-lg o-hidden border-0 my-5">

                    <div class="card-body p-0">

                        <div class="row">

                            <div class="col-lg-6 d-none d-lg-flex">

                                <div class="flex-grow-1 bg-login-image" style="background: url('img/logo.jpg') center/cover no-repeat, #f6f6f6; background-size: 400px;"></div>

                            </div>

                            <div class="col-lg-6">

                                <div class="p-5">


                                    <h3 class="text-center mb-4">Welcome Back!</h3>
                                    
                                    <h5 id="login_response" class="text-center mb-4 text-success"></h5>

                                    <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) ?>" class="form-group">

                                        <div class="form-group">
                                            <input type="text" name="user_name" class="form-control form-control-user" id="username_field" placeholder="Username" autofocus>
                                        </div>

                                        <div class="form-group">
                                            <input type="password" name="user_pass" class="form-control form-control-user" id="password_field" placeholder="Password">
                                        </div>

                                        <div class="form-group">
                                            <input type="button" name="login" class="btn btn-primary btn-block btn-user" id="login_btn" value="Login">
                                        </div>

                                    </form>


                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>


    <!-- Jquery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>

    <!-- custom scripts -->
    <script>
        
        $(document).ready(function() {
            
            $("#login_btn").click(function(){
                
                var username = $("#username_field").val();
                var password = $("#password_field").val();
                
                if(username == ""){
                    document.getElementById("username_field").style.border="2px solid red";
                    document.getElementById("username_field").focus();
                }else{
                    
                    document.getElementById("username_field").style.border="2px solid green";
                    
                }
                
                if(password == ""){
                    document.getElementById("password_field").style.border="2px solid red";
                    document.getElementById("password_field").focus();
                }else{
                    
                    document.getElementById("password_field").style.border="2px solid green";
                    
                }
                
                $.ajax({
                    
                    url:"./includes/login_script",
                    method:"POST",
                    cache: false,
                    data:{
                        action:"LOGIN",
                        username: username,
                        password:password
                    },
                    success:function(LoginResult){
                        
                        $("#login_response").html(LoginResult);
                        // window.location="admin/";
                        setTimeout(function(){
                            window.location.href = "admin/?logging successful";
                        }, 3000)
                        console.log("Logging successful");
                        
                    }
                    
                });
                
            });
            
            
        })
        
    </script>

</body>

</html>